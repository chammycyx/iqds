@echo off

if "%1" == "" goto usage

mvn release:clean release:prepare release:perform -DreleaseVersion=%1

goto end

:usage
echo usage: release.bat [releaseVersion]

:end
