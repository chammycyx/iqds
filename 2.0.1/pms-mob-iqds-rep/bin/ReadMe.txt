IQDS Replicator Deployment Guide
================================

Prerequisite
------------

1. Java latest version of JDK 1.6 (32bit or 64bit)


Installation steps
------------------

1. extract \\dc5winis62\Release\application\pms-mob-iqds-rep\pms-mob-iqds-rep-<latest-version>.zip into C:\Program Files

2. change dir to C:\Program Files\pms-mob-iqds-rep-<latest-version>

3. edit pms-mob-iqds-rep.properties and update the following lines 

  # The DMZ Windows IQDS server application url
  iqds-server-url=http://<dmz-iqds-server>/pms-mob-iqds/1

  # The current workstore information
  iqds-hosp-code=<hosp-code>
  iqds-workstore-code=<workstore-code>

4. install pms-mob-iqds-rep as a windows service by run the InstallService.bat

5. finally start the service by run the StartService.bat


Uninstallation steps
--------------------

1. change dir to C:\Program Files\pms-mob-iqds-rep-<current-version>

2. stop the service if still running by run the StopService.bat

3. uninstall the pms-mob-iqds-rep windows service by run the UninstallService.bat