package hk.org.ha.pms.mob.iqds.rep;

import hk.org.ha.pms.mob.iqds.util.IOUtils;
import hk.org.ha.pms.mob.iqds.util.SimpleLogger;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class IqdsRestClient {

	public static final int ISSUE = 1;
	public static final int COLLECT = 2;
	public static final String[] TYPE = {"none","issue","collect"};
	
	private SimpleLogger log;
	private String iqdsServerUrl;
	private String hospCode;
	private String workstoreCode;
	private int timeout;
	private int retryCount;
	private int retryDelayTime;
	
	public IqdsRestClient(SimpleLogger log, String iqdsServerUrl, String hospCode,
			String workstoreCode, int timeout, int retryCount, int retryDelayTime) {
		super();
		this.log = log;
		this.iqdsServerUrl = iqdsServerUrl;
		this.hospCode = hospCode;
		this.workstoreCode = workstoreCode;
		this.timeout = timeout;
		this.retryCount = retryCount;
		this.retryDelayTime = retryDelayTime;
	}
	
	public int send(int type, int ticketNum, int issueWindowNum) throws IOException 
	{		
    	int retry = retryCount;    	
    	do {    			    	
            try {
            	return internalSend(type, ticketNum, issueWindowNum);
			} catch (IOException e) { 
				if (retry-- > 0) {
					try {
						log.info("unable to send message, retry after " + retryDelayTime + "ms," +
								" retryCount:" + (retryCount-retry) + "/" + retryCount +
								" detail:" + e.getMessage());    
						Thread.sleep(retryDelayTime);
					} catch (InterruptedException e1) {}					
                } else {
                	throw e;
                }
			}
    	} while (true);			
	}
	
	private int internalSend(int type, int ticketNum, int issueWindowNum) throws IOException 
	{
		URL url = new URL(iqdsServerUrl + "/ticket/" +ticketNum + "/" + TYPE[type] + "?issueWindowNum=" + issueWindowNum);
		
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setConnectTimeout(timeout);
		conn.setRequestMethod("PUT");
		conn.setRequestProperty("HospCode", hospCode);
		conn.setRequestProperty("WorkstoreCode", workstoreCode);
		conn.setInstanceFollowRedirects(false);

		InputStream in = null;
		try {
			conn.connect();
			
			in  = conn.getInputStream();
			
			while (in.read() != -1);
			
			return conn.getResponseCode();
			
		} finally {
			IOUtils.closeQuietly(in);
			conn.disconnect();
		}		
	}
}
