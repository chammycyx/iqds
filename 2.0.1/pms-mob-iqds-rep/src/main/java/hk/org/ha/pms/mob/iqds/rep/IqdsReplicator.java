package hk.org.ha.pms.mob.iqds.rep;

import hk.org.ha.pms.mob.iqds.util.IOUtils;
import hk.org.ha.pms.mob.iqds.util.SimpleLogger;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Properties;

public class IqdsReplicator {

    public static final int DEFAULT_TIMEOUT = 10000;
    public static final int DEFAULT_RETRY_COUNT = 3;
    public static final int DEFAULT_RETRY_DELAY_TIME = 10000;
	
    public static final String APP_CODE = "pms-mob-iqds-rep";
    
    public static final String SERVER_HOST = "server-host";
    public static final String SERVER_PORT = "server-port";
    public static final String REPLICATE_HOST = "replicate-host";
    public static final String REPLICATE_PORT = "replicate-port";
    public static final String REPLICATE_TIMEOUT = "replicate-timeout";
    public static final String IQDS_SERVER_URL = "iqds-server-url";
    public static final String IQDS_SERVER_TIMEOUT = "iqds-server-timeout";
    public static final String IQDS_SERVER_RETRY_COUNT = "iqds-server-retry-count";
    public static final String IQDS_SERVER_RETRY_DELAY_TIME = "iqds-server-retry-delay-time";
    public static final String IQDS_HOSP_CODE = "iqds-hosp-code";
    public static final String IQDS_WORKSTORE_CODE = "iqds-workstore-code";
    
    private static ServerSocket serverSocket = null;
    private static SimpleLogger log = null;

    private static boolean started = false;
      	        
	public static void main(String[] args) throws Exception {
		
		Properties props = loadProperties();
		
		String hospCode = props.getProperty(IQDS_HOSP_CODE);
		String workstoreCode = props.getProperty(IQDS_WORKSTORE_CODE);
				
		log = new SimpleLogger(hospCode, workstoreCode);
				
        String _serverHost = props.getProperty(SERVER_HOST);
        String serverHost = isEmpty(_serverHost) ? null : _serverHost;
        int serverPort = Integer.parseInt(props.getProperty(SERVER_PORT));

        log.info("Server Host: " + serverHost);
        log.info("Server Port: " + serverPort);
        
        String _replicateHost = props.getProperty(REPLICATE_HOST);
        String replicateHost = isEmpty(_replicateHost) ? null : _replicateHost;
        String _replicatePort = props.getProperty(REPLICATE_PORT);
        int replicatePort = isEmpty(_replicatePort) ? 0 : Integer.parseInt(_replicatePort);
        String _replicateTimeout = props.getProperty(REPLICATE_TIMEOUT);
        int replicateTimeout = isEmpty(_replicateTimeout) ? DEFAULT_TIMEOUT : Integer.parseInt(_replicateTimeout);
        
        InetSocketAddress replicateAddress = null;
        if (replicatePort > 0) {
        	replicateAddress = new InetSocketAddress(InetAddress.getByName(replicateHost), replicatePort);

        	log.info("Replicate Host: " + replicateHost);
        	log.info("Replicate Port: " + replicatePort);
        	log.info("Replicate Timeout: " + replicateTimeout);
        }
        
        Socket socket = null;
        
        String iqdsServerUrl = props.getProperty(IQDS_SERVER_URL);
        
        String _iqdsServerTimeout = props.getProperty(IQDS_SERVER_TIMEOUT);
        int iqdsServerTimeout = isEmpty(_iqdsServerTimeout) ? DEFAULT_TIMEOUT : Integer.parseInt(_iqdsServerTimeout);
        String _iqdsServerRetryCount = props.getProperty(IQDS_SERVER_RETRY_COUNT);
        int iqdsServerRetryCount = isEmpty(_iqdsServerRetryCount) ? DEFAULT_RETRY_COUNT : Integer.parseInt(_iqdsServerRetryCount);
        String _iqdsServerRetryDelayTime = props.getProperty(IQDS_SERVER_RETRY_DELAY_TIME);
        int iqdsServerRetryDelayTime = isEmpty(_iqdsServerRetryDelayTime) ? DEFAULT_RETRY_DELAY_TIME : Integer.parseInt(_iqdsServerRetryDelayTime);
        
        log.info("Iqds Server Url: " + iqdsServerUrl);
        log.info("Iqds Server Timeout: " + iqdsServerTimeout);
        log.info("Iqds Server Retry Count: " + iqdsServerRetryCount);
        log.info("Iqds Server Retry Delay Time: " + iqdsServerRetryDelayTime);
        
        IqdsRestClient iqdsRestClient = null;
        if (!isEmpty(iqdsServerUrl)) {
        	iqdsRestClient = new IqdsRestClient(log, iqdsServerUrl, hospCode, workstoreCode, 
        			iqdsServerTimeout, iqdsServerRetryCount, iqdsServerRetryDelayTime);
        }
        
		started = true;
		log.info("Iqds Replicator started.");

    	serverSocket = new ServerSocket(serverPort, 0, InetAddress.getByName(serverHost));
		try {
			while (true) { 
			    socket = serverSocket.accept();
			    IqdsSocketHandler socketHandler = new IqdsSocketHandler(
			    		log, socket, replicateAddress, replicateTimeout, iqdsRestClient);
			    	        
			    Thread t = new Thread(socketHandler);
			    t.start();
			}
		} catch (SocketException e) {
			if (started) {
				throw e;
			}
		}
	}
	
    public static void start(String[] args) {
    	try {
			main(args);
		} catch (Exception e) {
			log.error("Iqds Replicator terminated unexpectedly.");
			e.printStackTrace();
		}
    }
    
    public static void stop(String[] args) {
    	
    	started = false;
		IOUtils.closeQuietly(serverSocket);
		
		log.info("Iqds Replicator stopped.");			
    }
	
    public static Properties loadProperties() throws IOException {

    	Properties p = new Properties();
    	String propFile = System.getProperty(APP_CODE + ".config");
    	if (propFile == null) {
    		propFile=APP_CODE + ".properties";
    	}

    	File file = new File(propFile);
    	InputStream is = null;
    	if (file.exists()) {
	    	is = new FileInputStream( propFile );
    	} else {
    		is = Thread.currentThread().getContextClassLoader().getResourceAsStream(propFile);
    	}
    	p.load(is);
    	is.close();

    	return p;
    }
        
    public static boolean isEmpty(String s) {
    	return s == null || s.trim().length() == 0;
    }
}
    

