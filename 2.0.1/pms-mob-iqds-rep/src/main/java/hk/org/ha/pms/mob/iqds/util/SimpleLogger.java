package hk.org.ha.pms.mob.iqds.util;

import java.io.PrintStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SimpleLogger {

	private DateFormat df = new SimpleDateFormat("yyyyMMdd.HHmmss");	

	private String hospCode;
	private String workstoreCode;
		
	public SimpleLogger(String hospCode, String workstoreCode) {
		super();
		this.hospCode = hospCode;
		this.workstoreCode = workstoreCode;
	}

	public  void info(String message) {
        print(System.out, message);
    }

	public void error(String message) {
        print(System.err, message);
    }

	private synchronized void print(PrintStream ps, String message) {
		ps.println(df.format(new Date()) + ": [" + Thread.currentThread().getId() + "]: " + hospCode + "," + workstoreCode + ": " + message);
    }
}
