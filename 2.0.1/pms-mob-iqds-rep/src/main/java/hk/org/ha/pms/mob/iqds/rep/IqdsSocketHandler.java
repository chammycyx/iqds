package hk.org.ha.pms.mob.iqds.rep;

import hk.org.ha.pms.mob.iqds.util.IOUtils;
import hk.org.ha.pms.mob.iqds.util.SimpleLogger;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;

public class IqdsSocketHandler implements Runnable {
	
	private SimpleLogger log;
	private Socket server;
	private InetSocketAddress repAddress;
	private int repTimeout;
	private IqdsRestClient iqdsRestClient;
	
	IqdsSocketHandler(SimpleLogger log, Socket server, InetSocketAddress repAddress, int repTimeout, IqdsRestClient iqdsRestClient) {
		this.log = log;
		this.server = server;
		this.repAddress = repAddress;
		this.repTimeout = repTimeout;
		this.iqdsRestClient = iqdsRestClient;
	}

	@Override
	public void run() {
		InputStream in = null;
		try {
			log.info("Receive data from remote address:" + server.getRemoteSocketAddress());
			
			in = server.getInputStream();
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			IOUtils.copy(in, baos);

			if (baos.size() == 0) {				
				log.info("Incoming iqds message is empty, (this may due to open and close socket without data)");
				return;
			}
			
			String _data = baos.toString("8859_1");
			
			if (repAddress != null) {
				Socket repSocket = null;
				OutputStream out = null;
				try {
					repSocket = new Socket();
					repSocket.connect(repAddress, repTimeout);
					out = repSocket.getOutputStream();
					out.write(baos.toByteArray());
					out.flush();
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					IOUtils.closeQuietly(out);
					IOUtils.closeQuietly(repSocket);
				}
				
				log.info("Iqds message [" + _data + "] replicated to " + repAddress.getHostName() + ":" + repAddress.getPort());
			}
			
			String data = _data.trim();
						
			if (data.length() < 2 || data.length() > 7) {
				throw new IllegalArgumentException("Incorrect length of iqds message [" + data + "]");
			}

			if (iqdsRestClient != null) 
			{
				int type = 0;
				String _type = data.substring(0, 1);
				if ("C".equals(_type)) {
					type = IqdsRestClient.ISSUE;
				} else if ("D".equals(_type)) {
					type = IqdsRestClient.COLLECT;
				}
				
				int ticketNum = 0;
				if (data.length() > 5) {
					ticketNum = Integer.parseInt(data.substring(1, 5).trim());
				} else {
					ticketNum = Integer.parseInt(data.substring(1).trim());
				}
				
				int issueWindowNum = 0;
				if (data.length() > 5) {
					issueWindowNum = Integer.parseInt(data.substring(5).trim());
				}
				
				if (type <= 0 || ticketNum < 0 || issueWindowNum < 0) {
					throw new IllegalArgumentException("Incorrect format of iqds message [" + data + "] {" + IqdsRestClient.TYPE[type] + "," + ticketNum + "," + issueWindowNum + "}");
				}
				
				int responseCode = iqdsRestClient.send(type, ticketNum, issueWindowNum);

				log.info("Iqds message {" + IqdsRestClient.TYPE[type] + "," + ticketNum + "," + issueWindowNum +"} submitted to REST Server, responseCode:" + responseCode);
			}
						
		} catch (IOException e) {
			String errorMsg = "Unexpected exception occur!";
			log.info(errorMsg + " : " + e.getMessage());
			log.error(errorMsg);
			e.printStackTrace();
		} finally {
			IOUtils.closeQuietly(server);
		}
	}	    
}