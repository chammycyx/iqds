package hk.org.ha.model.pms.mob.iqds.biz.push;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.mob.iqds.persistence.Device;
import hk.org.ha.model.pms.mob.iqds.persistence.DeviceType;
import hk.org.ha.model.pms.mob.iqds.persistence.Lang;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("deviceManager")
@MeasureCalls
public class DeviceManagerBean implements DeviceManagerLocal {
	
	@PersistenceContext
	private EntityManager em;

	@SuppressWarnings("unchecked")
	public Device retrieveDevice(String deviceUuid, DeviceType deviceType) 
	{
		List<Device> list = (List<Device>) em.createQuery(
				"select o from Device o" + // 20121023 index check : Device.uuid : I_DEVICE_01
				" where o.uuid = :uuid" +
				" and o.type = :type")
				.setParameter("uuid", deviceUuid)
				.setParameter("type", deviceType)
				.getResultList();
				
		return list.isEmpty() ? null : list.get(0);
	}
	
	public Device retrieveOrSaveDevice(String deviceUuid, String deviceToken, DeviceType deviceType) 
	{
		return retrieveOrSaveDevice(deviceUuid, deviceToken, deviceType, Lang.EN);
	}
	
	public Device retrieveOrSaveDevice(String deviceUuid, String deviceToken, DeviceType deviceType, Lang lang) 
	{
		Device device = retrieveDevice(deviceUuid, deviceType);

		if (device == null) {
			
			device = new Device();
			device.setUuid(deviceUuid);
			device.setToken(deviceToken);
			device.setType(deviceType);			
			device.setLang(lang);
			
			em.persist(device);
			
		} else {
			
			device.setToken(deviceToken);
			device.setLang(lang);
		}

		em.flush();
		
		return device;
	}
	
}
