package hk.org.ha.model.pms.mob.iqds.persistence;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum AccessCountLogType implements StringValuedEnum {

	DrugItem("DI", "Drug Document Item");	
	
    private final String dataValue;
    private final String displayValue;

    AccessCountLogType(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static AccessCountLogType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(AccessCountLogType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<AccessCountLogType> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<AccessCountLogType> getEnumClass() {
    		return AccessCountLogType.class;
    	}
    }
}



