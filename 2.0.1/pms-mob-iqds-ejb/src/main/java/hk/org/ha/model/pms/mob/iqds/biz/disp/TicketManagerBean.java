package hk.org.ha.model.pms.mob.iqds.biz.disp;

import hk.org.ha.fmk.pms.cache.CacheProvider;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.mob.iqds.biz.AppConstant;
import hk.org.ha.model.pms.mob.iqds.biz.push.PushRequestManagerLocal;
import hk.org.ha.model.pms.mob.iqds.persistence.Ticket;
import hk.org.ha.model.pms.mob.iqds.persistence.TicketStatus;
import hk.org.ha.model.pms.mob.iqds.persistence.Workstore;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import net.sf.ehcache.CacheManager;

import org.eclipse.persistence.config.PessimisticLock;
import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateless
@Name("ticketManager")
@MeasureCalls
public class TicketManagerBean implements TicketManagerLocal {

	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;
	
	@In
	private PushRequestManagerLocal pushRequestManager;
	
	@In
	private CacheProvider<CacheManager> cacheProvider;
		
	private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	@SuppressWarnings("unchecked")
	public Collection<Ticket> retrieveTickets(Workstore workstore, Date ticketDate) 
	{
		return (Collection<Ticket>) em.createQuery(
				"select o from Ticket o" + // 20121023 index check : Ticket.workstore,ticketDate : I_TICKET_01
				" where o.workstore = :workstore" +
				" and o.ticketDate = :ticketDate" +
				" and o.status = :ticketStatus" +
				" and o.issueDate is not null" +
				" and o.collectDate is null" +
				" order by o.issueDate")
				.setParameter("workstore", workstore)
				.setParameter("ticketDate", ticketDate)
				.setParameter("ticketStatus", TicketStatus.Active)
				.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Ticket> retrievePendingTickets(Workstore workstore, Date ticketDate, List<Integer> issueWinNumList){
		if( issueWinNumList.isEmpty() ){
			return new ArrayList<Ticket>();
		}else{		
			return (List<Ticket>)em.createQuery(
					"select o from Ticket o" + // 20141020 index check : Ticket.workstore,ticketDate,status,issueWindowNum : I_TICKET_02
					" where o.workstore = :workstore" +
					" and o.ticketDate = :ticketDate" +
					" and o.status = :ticketStatus" +
					" and o.issueWindowNum in :issueWindowNumList" +
					" order by o.issueDate")
					.setParameter("workstore", workstore)
					.setParameter("ticketDate", ticketDate)
					.setParameter("ticketStatus", TicketStatus.Pending)
					.setParameter("issueWindowNumList", issueWinNumList)
					.getResultList();
		}
	}
	
	public Ticket retrieveAndLockTicket(Workstore workstore, Date ticketDate, Integer ticketNum) 
	{
		return retrieveTicket(workstore, ticketDate, ticketNum, TicketStatus.Pending_Active,  true);
	}
	
	public Ticket retrieveAndLockTicket(Workstore workstore, Date ticketDate, Integer ticketNum, List<TicketStatus> ticketStatusList){
		return retrieveTicket(workstore, ticketDate, ticketNum, ticketStatusList,  true);
	}

	@SuppressWarnings("unchecked")
	private Ticket retrieveTicket(Workstore workstore, Date ticketDate, Integer ticketNum, List<TicketStatus> ticketStatusList, boolean lock) 
	{
		Query query = em.createQuery(
				"select o from Ticket o" + // 20121023 index check : Ticket.workstore,ticketDate,ticketNum : I_TICKET_01
				" where o.workstore = :workstore" +
				" and o.ticketDate = :ticketDate" +
				" and o.ticketNum = :ticketNum" +
				" and o.status in :ticketStatusList")
				.setParameter("workstore", workstore)
				.setParameter("ticketDate", ticketDate)
				.setParameter("ticketNum", ticketNum)
				.setParameter("ticketStatusList",ticketStatusList);
		
		if (lock) {
			query.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock);
		}
		
		List<Ticket> list = (List<Ticket>) query.getResultList();		
		return list.isEmpty() ? null : list.get(0);
	}

	// requires new only need for jboss without JBossTS (JTS)
	//@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void issueTicket(Workstore workstore, Date ticketDate, Integer ticketNum, Integer issueWindowNum) 
	{
		Ticket ticket = retrieveAndLockTicket(workstore, ticketDate, ticketNum);
		
		if (ticket == null) {		
			ticket = new Ticket();
			ticket.setWorkstore(workstore);
			ticket.setTicketDate(ticketDate);
			ticket.setTicketNum(ticketNum);
			ticket.setIssueWindowNum(issueWindowNum);
			ticket.setIssueDate(new Date());
			ticket.setStatus(TicketStatus.Pending);
			em.persist(ticket);
		} else {
			ticket.setIssueWindowNum(issueWindowNum);
			ticket.setIssueDate(new Date());
			ticket.setCollectDate(null);
			ticket.setStatus(TicketStatus.Pending);
		}
		
		em.flush();		
	}
	
	public boolean collectTicket(Workstore workstore, Date ticketDate, Integer ticketNum) 
	{
		String warnMsg = null;
		boolean updatePendingTicketStatus = false;
		Ticket ticket = retrieveAndLockTicket(workstore, ticketDate, ticketNum);

		if (ticket != null) {
			if (AppConstant.ENQUIRY_WINDOW_NUM_LIST.contains(ticket.getIssueWindowNum())) {
				
				pushRequestManager.unlinkPushRequests(ticket);

				em.remove(ticket);
				em.flush();
			} else {
				if (ticket.getCollectDate() == null) {
					if( ticket.getStatus() == TicketStatus.Pending ){
						ticket.setIssueDate(new Date());
						ticket.setStatus(TicketStatus.Active);
						updatePendingTicketStatus = true;
					}
					ticket.setCollectDate(new Date());
					em.flush();
				} else {
					warnMsg = "unable to collect: already collected on " + 
						dateFormat.format(ticket.getCollectDate());
				}
			}
		} else {
			warnMsg = "unable to collect: ticket not found";
		}

		if (warnMsg != null) {
			logger.warn(warnMsg + " [#0,#1,#2,#3]", 
					workstore.getHospCode(), 
					workstore.getWorkstoreCode(), 
					dateFormat.format(ticketDate),
					ticketNum);
		}
		return updatePendingTicketStatus;
	}
	
	public void invalidDispBoard(String hospCode, String workstoreCode) {
		
		logger.debug("invalidDispBoard for Workstore #0,#1", hospCode, workstoreCode);
		
		String key = hospCode + "_" + workstoreCode;
		cacheProvider.remove(AppConstant.DISP_BOARD_REGION, key);
	}	

}
