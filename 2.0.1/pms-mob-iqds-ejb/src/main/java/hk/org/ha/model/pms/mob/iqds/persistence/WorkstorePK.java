package hk.org.ha.model.pms.mob.iqds.persistence;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class WorkstorePK implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String hospCode;
	private String workstoreCode;
	
	public WorkstorePK() {
	}

	public WorkstorePK(String hospCode, String workstoreCode) {
		this.hospCode = hospCode;
		this.workstoreCode = workstoreCode;
	}

	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public String getWorkstoreCode() {
		return workstoreCode;
	}

	public void setWorkstoreCode(String workstoreCode) {
		this.workstoreCode = workstoreCode;
	}
	
	public boolean equals(Object other) {
		return EqualsBuilder.reflectionEquals(this, other);
	}
	
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
}
