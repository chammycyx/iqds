package hk.org.ha.model.pms.mob.iqds.persistence;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "DRUG_DOC_MAPPING")
public class DrugDocMapping {

	@Id
	@Column(name = "ITEM_CODE")
	private String itemCode;
		
	@ManyToOne
	@JoinColumn(name = "DRUG_NAME", nullable = false)
	private DrugDoc drugDoc;


	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public DrugDoc getDrugDoc() {
		return drugDoc;
	}

	public void setDrugDoc(DrugDoc drugDoc) {
		this.drugDoc = drugDoc;
	}

}
