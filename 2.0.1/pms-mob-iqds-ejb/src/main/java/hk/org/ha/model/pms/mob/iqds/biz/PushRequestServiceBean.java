package hk.org.ha.model.pms.mob.iqds.biz;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.mob.iqds.biz.AppConstant;
import hk.org.ha.model.pms.mob.iqds.biz.disp.TicketManagerLocal;
import hk.org.ha.model.pms.mob.iqds.biz.push.DeviceManagerLocal;
import hk.org.ha.model.pms.mob.iqds.biz.push.PushRequestManagerLocal;
import hk.org.ha.model.pms.mob.iqds.biz.reftable.WorkstoreManagerLocal;
import hk.org.ha.model.pms.mob.iqds.persistence.Device;
import hk.org.ha.model.pms.mob.iqds.persistence.DeviceType;
import hk.org.ha.model.pms.mob.iqds.persistence.Lang;
import hk.org.ha.model.pms.mob.iqds.persistence.RecordStatus;
import hk.org.ha.model.pms.mob.iqds.persistence.Ticket;
import hk.org.ha.model.pms.mob.iqds.persistence.TicketStatus;
import hk.org.ha.model.pms.mob.iqds.persistence.Workstore;
import hk.org.ha.model.pms.mob.iqds.vo.PushRequest;
import hk.org.ha.model.pms.mob.iqds.vo.PushRequestStatus;
import hk.org.ha.model.pms.mob.iqds.vo.ReturnResult;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;

import javax.ejb.Stateless;

import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.joda.time.DateMidnight;
import org.joda.time.DateTime;

@Stateless
@Name("pushRequestService")
@MeasureCalls
public class PushRequestServiceBean implements PushRequestServiceLocal {
		
	@In
	private TicketManagerLocal ticketManager;	

	@In
	private DeviceManagerLocal deviceManager;

	@In
	private PushRequestManagerLocal pushRequestManager;
	
	@In
	private WorkstoreManagerLocal workstoreManager;
	
	@Override
	public Collection<PushRequest> retrievePushRequests(
			String deviceUuid,
			String deviceToken,
			String deviceType,
			String lang) 
	{		
		Device device = deviceManager.retrieveOrSaveDevice(deviceUuid, deviceToken, DeviceType.dataValueOf(deviceType), Lang.dataValueOf(lang));		
		
		Date todayMidnight = new DateMidnight().toDate();
		
		Collection<PushRequest> ret = new ArrayList<PushRequest>();
		for (hk.org.ha.model.pms.mob.iqds.persistence.PushRequest 
				pr : pushRequestManager.retrievePushRequests(device, todayMidnight)) {
			
			PushRequest pushRequest = new PushRequest();
			pushRequest.setId(pr.getId());
			pushRequest.setWorkstore(pr.getWorkstore());
			pushRequest.setTicketDate(pr.getTicketDate());
			pushRequest.setTicketNum(pr.getTicketNum());
			
			if (pr.getStatus() == RecordStatus.Active) {
				pushRequest.setStatus(PushRequestStatus.Pending);
			} else {

				Ticket ticket = pr.getTicket();
				
				if (ticket != null) {
					
					pushRequest.setIssueWindowNum(ticket.getIssueWindowNum());		
					
					if (ticket.getCollectDate() != null) {
						pushRequest.setStatus(PushRequestStatus.Taken);						
					} else {
						if (AppConstant.ENQUIRY_WINDOW_NUM_LIST.contains(ticket.getIssueWindowNum())) {
							pushRequest.setStatus(PushRequestStatus.Enquiry);
						} else {
							pushRequest.setStatus(PushRequestStatus.Ready);
						}
					}
					
				} else {
					pushRequest.setStatus(PushRequestStatus.Pending);
				}
			}
			
			pushRequest.setStatusSortSeq(
					Integer.valueOf(pushRequest.getStatus().getSortSeq()));
			//For Phase 2 version or above
			pushRequest.setStatusSortSeqNew(
					Integer.valueOf(pushRequest.getStatus().getSortSeqNew()));
			
			if( pushRequest.getWorkstore().getStatus() == RecordStatus.Active ){
				ret.add(pushRequest);
			}
		}
				
		return ret;
	}
	
	@Override
	public ReturnResult addPushRequest(
			String deviceUuid,
			String deviceToken,
			String deviceType,
			String lang, 
			PushRequest pushRequest)
	{
		Date today = new Date();
		Date todayMidnight = new DateMidnight(today).toDate();
		
		Device device = deviceManager.retrieveOrSaveDevice(deviceUuid, deviceToken, DeviceType.dataValueOf(deviceType), Lang.valueOf(lang));
		Ticket ticket = ticketManager.retrieveAndLockTicket(pushRequest.getWorkstore(), todayMidnight, pushRequest.getTicketNum(), Arrays.asList(TicketStatus.Active));
		Workstore workstore = workstoreManager.retrieveWorkstore(pushRequest.getWorkstore().getHospCode(), pushRequest.getWorkstore().getWorkstoreCode());		
		
		if( workstore.getStatus() == RecordStatus.Active ){
			if (ticket != null) {
				if (ticket.getCollectDate() != null) {
					return new ReturnResult(-4, "ticket already collected");
				} else {
					
					if( !AppConstant.ENQUIRY_WINDOW_NUM_LIST.contains(ticket.getIssueWindowNum()) ){
						DateTime timeoutTime = null;
						if (workstore.getTimeoutInterval().intValue() > 0) {
							timeoutTime = new DateTime().minusMinutes(workstore.getTimeoutInterval().intValue());
						}
						
						if (timeoutTime != null && new DateTime(ticket.getIssueDate()).isBefore(timeoutTime)) {
							return new ReturnResult(-5, "ticket is timeout");
						}
					}
	
					return new ReturnResult(-3, "ticket is ready to collect", String.valueOf(ticket.getIssueWindowNum()));
				}
			} else {
				return pushRequestManager.savePushRequest(pushRequest.getWorkstore(), todayMidnight, pushRequest.getTicketNum(), device);
			}
		}else{
			return new ReturnResult(-6, "workstore is inactive");
		}
	}
	
	@Override
	public ReturnResult deletePushRequest(			
			String deviceUuid,
			String deviceToken,
			String deviceType,
			String lang, 
			Long pushRequestId) {
				
		Device device = deviceManager.retrieveDevice(deviceUuid, DeviceType.dataValueOf(deviceType));
		
		if (device == null) {
			return new ReturnResult(-4, "device not exist");
		} else {
			return pushRequestManager.deletePushRequest(device, pushRequestId);
		}
	}
}
