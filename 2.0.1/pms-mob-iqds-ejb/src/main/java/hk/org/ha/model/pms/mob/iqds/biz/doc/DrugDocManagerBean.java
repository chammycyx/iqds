package hk.org.ha.model.pms.mob.iqds.biz.doc;

import hk.org.ha.fmk.pms.util.Pair;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.mob.iqds.persistence.DrugDoc;
import hk.org.ha.model.pms.mob.iqds.persistence.DrugDocMapping;
import hk.org.ha.model.pms.mob.iqds.vo.ReturnResult;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateless
@Name("drugDocManager")
@MeasureCalls
public class DrugDocManagerBean implements DrugDocManagerLocal {

	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;	
	
	@Override
	public DrugDoc retrieveDrugDoc(String itemCode) {
		DrugDoc drugDoc = null;
		DrugDocMapping drugDocMapping = em.find(DrugDocMapping.class, itemCode);
		if ( drugDocMapping != null ) {
			drugDoc = drugDocMapping.getDrugDoc();
		}
		return drugDoc;
	}

	@Override
	public ReturnResult addDrugDoc(DrugDoc drugDoc) {
		DrugDoc tempDrugDoc = em.find(DrugDoc.class, drugDoc.getDrugName());
		if ( tempDrugDoc != null ) {
			logger.info("DrugDoc exist. drugName:#0", drugDoc.getDrugName());
			return new ReturnResult(-2, buildSystemMessageForList(tempDrugDoc));
		}
		Pair<Boolean, String> result = duplicateDrugDocMapping(drugDoc);
		if ( result.getFirst() == Boolean.TRUE ) {
			logger.info("Duplicate itemCode:#0", result.getSecond());
			return new ReturnResult(-3, buildSystemMessage(drugDoc.getDrugName(), result.getSecond()));
		}
		em.persist(drugDoc);
		return new ReturnResult(1, "Add successfully");
	}

	@Override
	public ReturnResult updateDrugDoc(DrugDoc drugDoc) {
		DrugDoc tempDrugDoc = em.find(DrugDoc.class, drugDoc.getDrugName());
		if ( tempDrugDoc == null ) {
			logger.info("DrugDoc not exist. drugName:#0", drugDoc.getDrugName());
			return new ReturnResult(-1, "DrugDoc not exist. drugName:"+drugDoc.getDrugName());
		}
		Pair<Boolean, String> result = duplicateDrugDocMapping(drugDoc);
		if ( result.getFirst() == Boolean.TRUE ) {
			logger.info("Duplicate itemCode:#0", result.getSecond());
			return new ReturnResult(-3, buildSystemMessage(drugDoc.getDrugName(), result.getSecond()));
		}
		em.merge(drugDoc);
		return new ReturnResult(1, "Update successfully");
	}

	@Override
	public ReturnResult deleteDrugDoc(String drugName) {
		DrugDoc drugDoc = em.find(DrugDoc.class, drugName);
		if ( drugDoc != null ) {
			em.remove(drugDoc);
			return new ReturnResult(1, "Successfully deleted");
		} 

		logger.info("DrugDoc not exist. drugName:#0", drugName);
		return new ReturnResult(-1, "DrugDoc not exist. drugName:"+drugName);
	}
	
	private Pair<Boolean, String> duplicateDrugDocMapping(DrugDoc drugDoc){
		for ( DrugDocMapping drugDocMapping : drugDoc.getDrugDocMappingList() ) {
			DrugDocMapping mapping = em.find(DrugDocMapping.class, drugDocMapping.getItemCode());
			if ( mapping != null && !StringUtils.equals(mapping.getDrugDoc().getDrugName(), drugDoc.getDrugName()) ) {
				return new Pair<Boolean, String>(Boolean.TRUE, drugDocMapping.getItemCode());
			}
			drugDocMapping.setDrugDoc(drugDoc);
		}
		return new Pair<Boolean, String>(Boolean.FALSE, "");
	}
	
	private String buildSystemMessage(String drugName, String itemCode){
		return itemCode +"("+drugName+")";
	}
	
	private String buildSystemMessageForList(DrugDoc drugDoc) {
		StringBuilder messageSb = new StringBuilder();
		for ( DrugDocMapping drugDocMapping : drugDoc.getDrugDocMappingList() ) {
			if ( messageSb.length() > 0 ) {
				messageSb.append("<br>");
			}
			messageSb.append(drugDocMapping.getItemCode()+"("+drugDoc.getDrugName()+")");
		}
		return messageSb.toString();
	}

}
