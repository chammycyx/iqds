package hk.org.ha.model.pms.mob.iqds.persistence;

import java.util.Arrays;
import java.util.List;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum TicketStatus implements StringValuedEnum {

	Pending("P", "Pending"),
	Active("A", "Active");
	
    private final String dataValue;
    private final String displayValue;
        
    public static final List<TicketStatus> Pending_Active = Arrays.asList(Pending,Active);
    
    TicketStatus(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
    
    public static TicketStatus dataValueOf(String dataValue) {
    	return StringValuedEnumReflect.getObjectFromValue(TicketStatus.class, dataValue);
    }
        
	public static class Converter extends StringValuedEnumConverter<TicketStatus> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<TicketStatus> getEnumClass() {
    		return TicketStatus.class;
    	}
    }
}
