package hk.org.ha.model.pms.mob.iqds.biz.reftable;

import hk.org.ha.model.pms.mob.iqds.persistence.DeviceType;
import java.util.Date;
import javax.ejb.Local;

@Local
public interface AccessCountLogManagerLocal {

	void countRetrieveDrugDoc(String itemCode, DeviceType deviceType, Date invokeDate);

}
