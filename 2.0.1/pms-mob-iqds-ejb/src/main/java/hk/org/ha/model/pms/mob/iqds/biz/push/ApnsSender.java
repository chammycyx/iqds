package hk.org.ha.model.pms.mob.iqds.biz.push;

import hk.org.ha.fmk.pms.util.ConfigHelper;
import hk.org.ha.fmk.pms.web.MeasureCalls;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javapns.communication.exceptions.CommunicationException;
import javapns.communication.exceptions.KeystoreException;
import javapns.devices.Device;
import javapns.devices.exceptions.InvalidDeviceTokenFormatException;
import javapns.devices.implementations.basic.BasicDevice;
import javapns.notification.AppleNotificationServer;
import javapns.notification.AppleNotificationServerBasicImpl;
import javapns.notification.Payload;
import javapns.notification.PushNotificationManager;
import javapns.notification.PushNotificationPayload;
import javapns.notification.PushedNotification;
import javapns.notification.PushedNotifications;
import javapns.notification.ResponsePacket;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Startup;
import org.jboss.seam.log.Log;

@Startup
@Name("apnsSender")
@Scope(ScopeType.APPLICATION)
@MeasureCalls
public class ApnsSender {
	
	@Logger
	private Log logger;
	
	private boolean enable = false;

	private String keyStore;
	
	private String keyStorePassword;
	
	private boolean production = false;
	    
	private int socketTimeout = 60000;
	
	private int retryCount = 3;
	private long retryDelayTime = 2000L;	
		    
    private boolean activated = false;    
    private PushNotificationManager pnManager = null;

    private boolean disabled = false;
    private String keyStoreCanonicalPath = null;    
    
    private long lastActivityTime = 0L;
    
    private String sound = "default";    

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	public String getKeyStore() {
		return keyStore;
	}

	public void setKeyStore(String keyStore) {
		this.keyStore = keyStore;
	}

	public String getKeyStorePassword() {
		return keyStorePassword;
	}

	public void setKeyStorePassword(String keyStorePassword) {
		this.keyStorePassword = keyStorePassword;
	}

	public boolean isProduction() {
		return production;
	}

	public void setProduction(boolean production) {
		this.production = production;
	}

	public int getSocketTimeout() {
		return socketTimeout;
	}

	public void setSocketTimeout(int socketTimeout) {
		this.socketTimeout = socketTimeout;
	}	
	
	public int getRetryCount() {
		return retryCount;
	}

	public void setRetryCount(int retryCount) {
		this.retryCount = retryCount;
	}

	public long getRetryDelayTime() {
		return retryDelayTime;
	}

	public void setRetryDelayTime(long retryDelayTime) {
		this.retryDelayTime = retryDelayTime;
	}
	
	public String getSound() {
		return sound;
	}

	public void setSound(String sound) {
		this.sound = sound;
	}

	private String getKeyStoreCanonicalPath() throws IOException {
        File file = new File(keyStore);
        if (!file.exists()) {        	
	        File configFolder = ConfigHelper.getConfigFolder();
	        if (configFolder != null) {
	        	file = new File(configFolder, keyStore);
	        }
        }

        if (!file.exists()) {
    		throw new FileNotFoundException("unable to find keystore by " + keyStore);
    	}

        return file.getCanonicalPath();
	}

	@Create
	public void init() 
	{
		if (enable) {
			
			try {
				keyStoreCanonicalPath = this.getKeyStoreCanonicalPath();
			} catch (IOException e) {	
				logger.warn("missing keystore file, " + e.getMessage());
				disabled = true;
			}	
		}
	}
		
	private void activate() throws CommunicationException, KeystoreException, IOException 
	{		
        if (enable && !activated) {
        	        	
    		AppleNotificationServer apns = new AppleNotificationServerBasicImpl(
        			keyStoreCanonicalPath, 
        			keyStorePassword,
        			production);	
 
    		pnManager = new PushNotificationManager();
    		pnManager.setSslSocketTimeout(socketTimeout);
        	
        	pnManager.initializeConnection(apns);
        	lastActivityTime = System.currentTimeMillis();

        	activated = true;
        }
        
        // reinit the connection
		if ((System.currentTimeMillis() - lastActivityTime) > (socketTimeout - 1000)) {
			logger.info("socket timeout for #0ms, reinit connection!", socketTimeout);
			pnManager.stopConnection();
			pnManager.initializePreviousConnection();
        	lastActivityTime = System.currentTimeMillis();
		}
    }  
	
	public void sendNotifications(String message, List<String> deviceTokens) throws CommunicationException, KeystoreException, IOException, InvalidDeviceTokenFormatException {
				
		this.sendNotifications(PushNotificationPayload.alert(message), buildDeviceList(deviceTokens));
	}

	public void sendNotifications(String message, int badge, List<String> deviceTokens) throws CommunicationException, KeystoreException, IOException, InvalidDeviceTokenFormatException 
	{
		this.sendNotifications(PushNotificationPayload.combined(message, badge, sound), buildDeviceList(deviceTokens));		
	}
	
	public void sendNotifications(String message, int badge, String sound, List<String> deviceTokens) throws CommunicationException, KeystoreException, IOException, InvalidDeviceTokenFormatException {
		
		this.sendNotifications(PushNotificationPayload.combined(message, badge, sound), buildDeviceList(deviceTokens));		
	}
	
	private List<Device> buildDeviceList(List<String> deviceTokens) throws InvalidDeviceTokenFormatException {
		List<Device> devices = new ArrayList<Device>();
		for (String deviceToken : deviceTokens) {
			devices.add(new BasicDevice(deviceToken));
		}
		return devices;
	}
	
	public void sendNotifications(Payload payload, List<Device> devices) throws CommunicationException, KeystoreException, IOException {
		
		synchronized (this) {
					
			if (!enable || disabled) {
				logger.warn("ApnsSender is disabled, please check your configuration.");
				return;
			}
			
	    	int retry = retryCount;    	
	    	do {    			    	
	            try {

		    		activate();	    		
		    				    		
					if (logger.isDebugEnabled()) {
						logger.debug("message about to send through APNS (#0)", (production ? "production" : "sandbox"));
					}
		    		
				    PushedNotifications notifications = new PushedNotifications();
				    for (Device device : devices) {
				    	notifications.add(pnManager.sendNotification(device, payload, false));
						lastActivityTime = System.currentTimeMillis();
				    }
				    
				    for (PushedNotification notification : notifications) {
				    	if (notification.isSuccessful()) {
							logger.info("message sent successfully to #0", notification.getDevice().getToken());
				    	} else {
                            logger.warn("message fail to send #0, #1", notification.getDevice().getToken(), notification.getException().getMessage());

                            /* If the problem was an error-response packet returned by Apple, get it */  
                            ResponsePacket theErrorResponse = notification.getResponse();
                            if (theErrorResponse != null) {
                            	logger.warn("error-response packet by apple: #0", theErrorResponse.getMessage());
                            }
                        }
				    }
				    
					return;
					
				} catch (CommunicationException e1) { 
					if (retry-- > 0) {
						logAndSleep(retry, e1.getMessage());
				    	activated = false;
	                } else {
	                	throw e1;
	                }
				} catch (IOException e2) { 
					if (retry-- > 0) {
						logAndSleep(retry, e2.getMessage());
				    	activated = false;
	                } else {
	                	throw e2;
	                }
				}
	    	} while (true);

		}
	}
	
	private void logAndSleep(int retry, String detailMessage) {
		logger.warn("unable to send message, retry after " + retryDelayTime + "ms," +
				" retryCount:" + (retryCount-retry) + "/" + retryCount +
				" detail:" + detailMessage);    	
		try {
			Thread.sleep(retryDelayTime);
		} catch (InterruptedException e1) {}
	}
		
	@Destroy
	public void destroy() 
	{
		synchronized (this) {
			
			if (activated) {
				try {
					pnManager.stopConnection();
				} catch (CommunicationException e) {
				} catch (KeystoreException e) {
				}
				pnManager = null;
				activated = false;
			}
			keyStoreCanonicalPath = null;
			disabled = false;
		}
	}
}
