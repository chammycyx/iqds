package hk.org.ha.model.pms.mob.iqds.persistence;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class AccessCountLogPK  implements Serializable {

	private static final long serialVersionUID = 1L;

	private AccessCountLogType type;
	private String name;
	private Date logDate;
	private DeviceType deviceType;
	
	public AccessCountLogPK() {
	}

	public AccessCountLogType getType() {
		return type;
	}

	public void setType(AccessCountLogType type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Date getLogDate() {
		return logDate;
	}

	public void setLogDate(Date logDate) {
		this.logDate = logDate;
	}

	public DeviceType getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(DeviceType deviceType) {
		this.deviceType = deviceType;
	}
	
	public boolean equals(Object other) {
		return EqualsBuilder.reflectionEquals(this, other);
	}
	
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
}
