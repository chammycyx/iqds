package hk.org.ha.model.pms.mob.iqds.biz;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.mob.iqds.biz.reftable.WorkstoreManagerLocal;
import hk.org.ha.model.pms.mob.iqds.persistence.Workstore;

import java.util.Collection;

import javax.ejb.Stateless;

import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

@Stateless
@Name("workstoreService")
@MeasureCalls
public class WorkstoreServiceBean implements WorkstoreServiceLocal {

	@In
	private WorkstoreManagerLocal workstoreManager;
	
	public Collection<Workstore> retrieveWorkstores() {
		return workstoreManager.retrieveWorkstores();
	}
	
}
