package hk.org.ha.model.pms.mob.iqds.biz;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.mob.iqds.biz.reftable.InfoLinkManagerLocal;
import hk.org.ha.model.pms.mob.iqds.persistence.InfoLink;

import java.util.Collection;

import javax.ejb.Stateless;

import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

@Stateless
@Name("infoLinkService")
@MeasureCalls
public class InfoLinkServiceBean implements InfoLinkServiceLocal {

	@Logger Log logger;
		
	@In
	private InfoLinkManagerLocal infoLinkManager;

	public Collection<InfoLink> retrieveInfoLinks() {
		return infoLinkManager.retrieveInfoLinks();
	}	
}
