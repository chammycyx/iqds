package hk.org.ha.model.pms.mob.iqds.biz.push;
import hk.org.ha.model.pms.mob.iqds.persistence.Device;
import hk.org.ha.model.pms.mob.iqds.persistence.DeviceType;
import hk.org.ha.model.pms.mob.iqds.persistence.Lang;

import javax.ejb.Local;

@Local
public interface DeviceManagerLocal {

	Device retrieveDevice(String deviceUuid, DeviceType deviceType);

	Device retrieveOrSaveDevice(String deviceUuid, String deviceToken, DeviceType deviceType);
	
	Device retrieveOrSaveDevice(String deviceUuid, String deviceToken, DeviceType deviceType, Lang lang);
}
