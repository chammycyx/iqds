package hk.org.ha.model.pms.mob.iqds.biz;

import hk.org.ha.fmk.pms.util.Pair;
import hk.org.ha.fmk.pms.util.Resources;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.mob.iqds.persistence.DeviceType;
import hk.org.ha.model.pms.mob.iqds.vo.AppVersion;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.ejb.Stateless;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.annotations.Name;

@Stateless
@Name("appService")
@MeasureCalls
public class AppServiceBean implements AppServiceLocal {
	
	private static final String PHARMACY_MOB = "pharmacy-mob";
	private static final String COMPATIBLE_VERSION = "compatible-version";
	private static final String LATEST_VERSION = "latest-version";
	
	private static Map<Pair<DeviceType,Integer>,AppVersion> appVersionMap = new HashMap<Pair<DeviceType,Integer>,AppVersion>();
		
	@Override
	public AppVersion retrieveAppVersion(String deviceType, String deviceOSVersion) {
			DeviceType type = DeviceType.dataValueOf(deviceType);
			String[] deviceVersionSplit = StringUtils.split(deviceOSVersion, ".");
			Integer deviceVersion =	Integer.valueOf(deviceVersionSplit[0]);
			
			AppVersion appVersion = null;
			
			Pair<DeviceType,Integer> deviceVersionKey = new Pair<DeviceType,Integer>(type, deviceVersion);
			
			synchronized (appVersionMap)
			{
				appVersion = appVersionMap.get(deviceVersionKey);
				if (appVersion == null) {
					Properties properties = Resources.getComponentProperties();
					
					String keyPrefix = PHARMACY_MOB + "."+ type.getDisplayValue().toLowerCase() + "." + deviceVersion.toString() + ".";
					
					if( properties.get(keyPrefix + COMPATIBLE_VERSION) == null ){
						keyPrefix = PHARMACY_MOB + "." + type.getDisplayValue().toLowerCase() + ".";
					}
					appVersion = new AppVersion(
							(String) properties.get(keyPrefix + COMPATIBLE_VERSION),
							(String) properties.get(keyPrefix + LATEST_VERSION));
					appVersionMap.put(deviceVersionKey, appVersion);
				}
			}
			return appVersion;
	}
}
