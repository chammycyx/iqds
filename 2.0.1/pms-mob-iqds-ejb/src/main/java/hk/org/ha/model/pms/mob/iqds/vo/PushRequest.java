package hk.org.ha.model.pms.mob.iqds.vo;

import hk.org.ha.model.pms.mob.iqds.persistence.Workstore;

import java.util.Date;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PushRequest {

	private Long id;
	
	private Workstore workstore;
		
	private Date ticketDate;
	
	private Integer ticketNum;
	
	private PushRequestStatus status;
	
	private Integer statusSortSeq;
	
	private Integer statusSortSeqNew;
	
	private Integer issueWindowNum;
	
	public PushRequest() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Workstore getWorkstore() {
		return workstore;
	}

	public void setWorkstore(Workstore workstore) {
		this.workstore = workstore;
	}

	public Date getTicketDate() {
		return ticketDate;
	}

	public void setTicketDate(Date ticketDate) {
		this.ticketDate = ticketDate;
	}

	public Integer getTicketNum() {
		return ticketNum;
	}

	public void setTicketNum(Integer ticketNum) {
		this.ticketNum = ticketNum;
	}

	public PushRequestStatus getStatus() {
		return status;
	}

	public void setStatus(PushRequestStatus status) {
		this.status = status;
	}
	
	public Integer getStatusSortSeq() {
		return statusSortSeq;
	}

	public void setStatusSortSeq(Integer statusSortSeq) {
		this.statusSortSeq = statusSortSeq;
	}

	public Integer getIssueWindowNum() {
		return issueWindowNum;
	}

	public void setIssueWindowNum(Integer issueWindowNum) {
		this.issueWindowNum = issueWindowNum;
	}

	public void setStatusSortSeqNew(Integer statusSortSeqNew) {
		this.statusSortSeqNew = statusSortSeqNew;
	}

	public Integer getStatusSortSeqNew() {
		return statusSortSeqNew;
	}
}
