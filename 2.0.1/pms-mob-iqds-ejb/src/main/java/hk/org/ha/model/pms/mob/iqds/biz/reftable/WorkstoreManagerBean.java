package hk.org.ha.model.pms.mob.iqds.biz.reftable;

import hk.org.ha.fmk.pms.cache.CacheResult;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.mob.iqds.persistence.RecordStatus;
import hk.org.ha.model.pms.mob.iqds.persistence.Workstore;
import hk.org.ha.model.pms.mob.iqds.persistence.WorkstorePK;

import java.util.Collection;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("workstoreManager")
@MeasureCalls
public class WorkstoreManagerBean implements WorkstoreManagerLocal {
	@PersistenceContext
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	@CacheResult(timeout="60000")
	public Collection<Workstore> retrieveWorkstores() 
	{
		return (Collection<Workstore>) em.createQuery(
				"select o from Workstore o" + // 20121023 index check : none
				" where o.status = :status" +
				" order by o.area, o.name")
				.setParameter("status", RecordStatus.Active)
				.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Workstore> retrieveWorkstoresWithWindowTicketCount(){		
		return (List<Workstore>) em.createQuery(
				"select o from Workstore o" + // 20121023 index check : none
				" where o.status = :status and" +
				" o.windowTicketCount > 0" +
				" order by o.area, o.name"
				)
				.setParameter("status", RecordStatus.Active)
				.getResultList();
	}
	
	public Workstore retrieveWorkstore(String hospCode, String workstoreCode) 
	{
		if (hospCode == null) {
			throw new IllegalArgumentException("missing HospCode " + hospCode);
		}
		if (workstoreCode == null) {
			throw new IllegalArgumentException("missing WorkstoreCode " + workstoreCode);
		}

		Workstore workstore = em.find(Workstore.class, new WorkstorePK(hospCode, workstoreCode));		
		if (workstore == null) {
			throw new IllegalArgumentException("Workstore not found for " + hospCode + " " + workstoreCode);
		}
		return workstore;
	}
}
