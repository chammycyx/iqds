package hk.org.ha.model.pms.mob.iqds.persistence;

import hk.org.ha.fmk.pms.entity.CreateDate;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;

@Entity
@Table(name = "DEVICE")
public class Device {

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "deviceSeq")
	@SequenceGenerator(name = "deviceSeq", sequenceName = "SQ_DEVICE", initialValue = 100000000)
	private Long id;

	@Column(name = "UUID", nullable = false, length = 36)
	private String uuid;
	
	@Column(name = "TOKEN", length = 1000)
	private String token;
	
	@Converter(name = "Device.type", converterClass = DeviceType.Converter.class)
    @Convert("Device.type")
	@Column(name="TYPE", nullable = false, length = 1)
	private DeviceType type;
	
	@Converter(name = "Device.lang", converterClass = Lang.Converter.class)
    @Convert("Device.lang")
	@Column(name="LANG", nullable = false, length = 2)
	private Lang lang;

	@CreateDate
	@Column(name = "CREATE_DATE", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;
	
	public Device() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public DeviceType getType() {
		return type;
	}

	public void setType(DeviceType type) {
		this.type = type;
	}

	public Lang getLang() {
		return lang;
	}

	public void setLang(Lang lang) {
		this.lang = lang;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public boolean isMatch(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Device other = (Device) obj;
		if (token == null) {
			if (other.token != null)
				return false;
		} else if (!token.equals(other.token))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}
}
