package hk.org.ha.model.pms.mob.iqds.persistence;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;

@Entity
@Table(name = "TICKET")
public class Ticket {

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ticketSeq")
	@SequenceGenerator(name = "ticketSeq", sequenceName = "SQ_TICKET", initialValue = 100000000)
	private Long id;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumns({
		@JoinColumn(name = "HOSP_CODE", referencedColumnName = "HOSP_CODE", nullable = false),
		@JoinColumn(name = "WORKSTORE_CODE", referencedColumnName = "WORKSTORE_CODE", nullable = false)
	})
	private Workstore workstore;

	@Column(name = "TICKET_DATE", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date ticketDate;
	
	@Column(name = "TICKET_NUM", nullable = false)
	private Integer ticketNum;

	@Column(name = "ISSUE_WINDOW_NUM", nullable = false)
	private Integer issueWindowNum;
	
	@Column(name = "ISSUE_DATE", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date issueDate;
	
	@Column(name = "COLLECT_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date collectDate;

	@Converter(name = "Ticket.status", converterClass = TicketStatus.Converter.class)
    @Convert("Ticket.status")
	@Column(name = "STATUS", nullable = false, length = 1)
	private TicketStatus status;

	public Ticket(){
		status = TicketStatus.Pending;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Workstore getWorkstore() {
		return workstore;
	}

	public void setWorkstore(Workstore workstore) {
		this.workstore = workstore;
	}

	public Date getTicketDate() {
		return ticketDate;
	}

	public void setTicketDate(Date ticketDate) {
		this.ticketDate = ticketDate;
	}

	public Integer getIssueWindowNum() {
		return issueWindowNum;
	}

	public Integer getTicketNum() {
		return ticketNum;
	}

	public void setTicketNum(Integer ticketNum) {
		this.ticketNum = ticketNum;
	}

	public void setIssueWindowNum(Integer issueWindowNum) {
		this.issueWindowNum = issueWindowNum;
	}

	public Date getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}

	public Date getCollectDate() {
		return collectDate;
	}

	public void setCollectDate(Date collectDate) {
		this.collectDate = collectDate;
	}

	public TicketStatus getStatus() {
		return status;
	}
	
	public void setStatus(TicketStatus status) {
		this.status = status;
	}
}
