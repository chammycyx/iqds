package hk.org.ha.model.pms.mob.iqds.biz;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Startup;
import org.jboss.seam.async.TimerServiceDispatcher;

@Startup
@Name("iqdsStarter")
@Scope(ScopeType.APPLICATION)
public class IqdsStarter {
	
	@In
	private IqdsStartupLocal iqdsStartup;
	
	private int delayTime = 60000;
		
	public void setDelayTime(int delayTime) {
		this.delayTime = delayTime;
	}

	public int getDelayTime() {
		return delayTime;
	}

	@Create
	public void create() {
		TimerServiceDispatcher.instance().cancelAllTimers();
		iqdsStartup.startup(Long.valueOf(getDelayTime()));
	}
	
	@Destroy
    public void destroy() {
        TimerServiceDispatcher.instance().cancelAllTimers();
    }
}

