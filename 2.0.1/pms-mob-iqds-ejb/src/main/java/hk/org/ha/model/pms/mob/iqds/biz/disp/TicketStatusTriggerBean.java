package hk.org.ha.model.pms.mob.iqds.biz.disp;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.mob.iqds.biz.AppConstant;
import hk.org.ha.model.pms.mob.iqds.biz.DispBoardServiceLocal;
import hk.org.ha.model.pms.mob.iqds.biz.TimerProp;
import hk.org.ha.model.pms.mob.iqds.biz.reftable.WorkstoreManagerLocal;
import hk.org.ha.model.pms.mob.iqds.persistence.Ticket;
import hk.org.ha.model.pms.mob.iqds.persistence.TicketStatus;
import hk.org.ha.model.pms.mob.iqds.persistence.Workstore;
import hk.org.ha.model.pms.mob.iqds.persistence.WorkstorePK;
import hk.org.ha.model.pms.mob.iqds.vo.DispBoard;
import hk.org.ha.model.pms.mob.iqds.vo.DispWindow;
import hk.org.ha.service.biz.pms.iqds.interfaces.IqdsTicketProcessorJmsRemote;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.NotSupportedException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import org.eclipse.persistence.config.PessimisticLock;
import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;
import org.joda.time.DateMidnight;

@AutoCreate
@Stateless
@Name("ticketStatusTrigger")
@TransactionManagement(TransactionManagementType.BEAN)
@MeasureCalls
public class TicketStatusTriggerBean implements TicketStatusTriggerLocal {

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
		
	@In
	private TicketManagerLocal ticketManager;
	
	@In
	private WorkstoreManagerLocal workstoreManager;
	
	@In
	private DispBoardServiceLocal dispBoardService;
	
	@In
	private IqdsTicketProcessorJmsRemote iqdsTicketProcessorProxy;
	
	@In
	private TimerProp timerProp;
	
	@Resource
	private UserTransaction userTransaction;	
	
    public TicketStatusTriggerBean() {
        // TODO Auto-generated constructor stub
    }
    
	@Override
    public void processIssueTicketsByTimeout(Long duration, Long intervalDuration){
		if (timerProp.isActivate()) {
			List<Workstore> workstoreList = workstoreManager.retrieveWorkstoresWithWindowTicketCount();
    	
			for( Workstore workstore : workstoreList ){
				this.processIssuePendingTicketList(workstore, false);
			}
		}else{
			logger.warn("processIssueTicketsTimer temporary disabled by " + TimerProp.TIMER_PROPERTIES);
		}
    }
    
	@Override
    public void processIssueTicketByWorkstore(String hospCode, String workstoreCode){
		Workstore workstore = em.find(Workstore.class, new WorkstorePK(hospCode, workstoreCode));

    	this.processIssuePendingTicketList(workstore, true);
    }
    
	private void processIssuePendingTicketList(Workstore workstore, boolean invalidDispBoard){
		boolean success = false;
		Boolean forceDispBoardUpdate = false;		
		
			try {
					beginTransaction();
					
					Workstore task = lockWorkstore(workstore);
					
					if( task != null ){
						Map<Integer, Integer> vacantIssueWindowNumMap = retrieveAvailableIssueWindowNumMapByWorkstore(workstore, invalidDispBoard);
						
						List<Ticket> ticketList = ticketManager.retrievePendingTickets(workstore, new DateMidnight().toDate(), new ArrayList<Integer>(vacantIssueWindowNumMap.keySet()));
						while (!ticketList.isEmpty()){
						
							forceDispBoardUpdate = false;
							for(Ticket pendingTicket : ticketList){
								
								if( workstore.isBufferQueueEnabled() ){
									Integer remainCount = vacantIssueWindowNumMap.get(pendingTicket.getIssueWindowNum());
										
									if( remainCount == null ){
										continue;
									}else if( Integer.valueOf(1).equals(remainCount) ){
										forceDispBoardUpdate = true;
										vacantIssueWindowNumMap.remove(pendingTicket.getIssueWindowNum());
									}else{
										forceDispBoardUpdate = true;
										vacantIssueWindowNumMap.put(pendingTicket.getIssueWindowNum(), remainCount-1);
									}
								}
								
								logger.info("ProcessTicket:[#0,#1]Ticket:#2,IssueWinNum:#3", 
										workstore.getHospCode(), workstore.getWorkstoreCode(),
										pendingTicket.getTicketNum(), pendingTicket.getIssueWindowNum());
								pendingTicket.setStatus(TicketStatus.Active);
								pendingTicket.setIssueDate(new Date());
									
								iqdsTicketProcessorProxy.processTicket(workstore, pendingTicket.getIssueDate(), pendingTicket.getTicketNum());
							}
							ticketList = ticketManager.retrievePendingTickets(workstore, new DateMidnight().toDate(), new ArrayList<Integer>(vacantIssueWindowNumMap.keySet()));
						}
						success = true;
					}
			}catch (Exception ex) {
				String message = ex.getMessage();
				if (message != null && message.toLowerCase().indexOf("nowait") != -1) {
					logger.info("Processing state cannot be release for Workstore[#0,#1]," +
							" which is currently locked by another transaction.", workstore.getHospCode(), workstore.getWorkstoreCode());
				} else {
					logger.error(ex, ex);
				}
			} finally {
				endTransaction(success);
			}
			
		if( forceDispBoardUpdate ){
			ticketManager.invalidDispBoard(workstore.getHospCode(), workstore.getWorkstoreCode());
		}
	}
	

	
    private Map<Integer, Integer> retrieveAvailableIssueWindowNumMapByWorkstore(Workstore workstore, boolean invalidDispBoard){
    	//invalid DispBoard
    	if( invalidDispBoard ){
    		ticketManager.invalidDispBoard(workstore.getHospCode(), workstore.getWorkstoreCode());
    	}
    	
    	DispBoard dispBoard = dispBoardService.retrieveDispBoard(workstore.getHospCode(), workstore.getWorkstoreCode());
    	
    	Map<Integer, Integer> availableTicketTokenMap = new HashMap<Integer, Integer>();
		
    	for(Integer winNum: AppConstant.DEFAULT_WINDOW_NUM_LIST){
			availableTicketTokenMap.put(winNum, workstore.getWindowTicketCount());
		}
    	
    	if( workstore.isBufferQueueEnabled() ){
			for (Iterator<DispWindow> dispWindowItr = dispBoard.getDispWindows().iterator();dispWindowItr.hasNext();) {
				DispWindow dispWindow = dispWindowItr.next();
				
				if( availableTicketTokenMap.get(dispWindow.getIssueWindowNum()) != null ){
					Integer remainVacant = availableTicketTokenMap.get(dispWindow.getIssueWindowNum());
					if( dispWindow.getTickets().size() >= remainVacant ){
						availableTicketTokenMap.remove(dispWindow.getIssueWindowNum());
					}else{
						remainVacant = remainVacant - dispWindow.getTickets().size();
						availableTicketTokenMap.put(dispWindow.getIssueWindowNum(), remainVacant);
					}
				}
			}
    	}
    	
    	return availableTicketTokenMap;
    }
    
	private Workstore lockWorkstore(Workstore workstore) {
		
		Workstore lockWorkstore = (Workstore) em.createQuery(
								"select o from Workstore o" + // 20141020 index check : Workstore.PK : PK_WORKSTORE
								" where o.hospCode = :hospCode" +
								" and o.workstoreCode = :workstoreCode" +
								" and o.hospCode is not null" +
								" and o.workstoreCode is not null")
								.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.LockNoWait)
								.setParameter("hospCode", workstore.getHospCode())
								.setParameter("workstoreCode", workstore.getWorkstoreCode())
								.getSingleResult();
		
		return lockWorkstore;
	}
	
	private void beginTransaction() throws NotSupportedException, SystemException {
		beginTransaction(0);
	}
	
	private void beginTransaction(int timeout) throws NotSupportedException, SystemException {
		if (timeout > 0) {
			userTransaction.setTransactionTimeout(timeout);
		}
		userTransaction.begin();
	}
	
	private void endTransaction(boolean success) {
		if (success) {
			commit();
		} else {
			rollback();
		}
	}

	private void commit() {
		try {
			userTransaction.commit();
		} catch (Exception ex) {
		}
	}	
	
	private void rollback() {
		try {
			userTransaction.rollback();
		} catch (Exception ex) {
		}
	}	
}
