package hk.org.ha.model.pms.mob.iqds.persistence;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@Entity
@Table(name = "INFO_LINK")
@JsonIgnoreProperties(ignoreUnknown = true)
public class InfoLink {

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "infoLinkSeq")
	@SequenceGenerator(name = "infoLinkSeq", sequenceName = "SQ_INFO_LINK", initialValue = 100000000)
	private Long id;

	@Column(name = "NAME", nullable = false, length = 100)
	private String name;

	@Column(name = "NAME_TC", nullable = false, length = 300)
	private String nameTc;
	
	//obsolete after compatible verion IOS: 2.0.0, Andriod:1.0.3.1
	@Column(name = "URL", nullable = false, length = 150)
	private String url;
	
	@Column(name = "SORT_SEQ", nullable = false)
	private Integer sortSeq;

	@Column(name = "HYPERLINK", nullable = false, length = 150)
	private String hyperlink;
	
	@Column(name = "HYPERLINK_TC", nullable = false, length = 150)
	private String hyperlinkTc;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNameTc() {
		return nameTc;
	}

	public void setNameTc(String nameTc) {
		this.nameTc = nameTc;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getSortSeq() {
		return sortSeq;
	}

	public void setSortSeq(Integer sortSeq) {
		this.sortSeq = sortSeq;
	}

	public String getHyperlink() {
		return hyperlink;
	}
	
	public void setHyperlink(String hyperlink) {
		this.hyperlink = hyperlink;
	}

	public void setHyperlinkTc(String hyperlinkTc) {
		this.hyperlinkTc = hyperlinkTc;
	}

	public String getHyperlinkTc() {
		return hyperlinkTc;
	}	
}
