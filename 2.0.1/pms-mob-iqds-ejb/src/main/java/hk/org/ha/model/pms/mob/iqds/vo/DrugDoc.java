package hk.org.ha.model.pms.mob.iqds.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DrugDoc {

	private String drugName;
	
	private String drugDocHtml;
	
	public DrugDoc() {
		super();
	}

	public String getDrugName() {
		return drugName;
	}

	public void setDrugName(String drugName) {
		this.drugName = drugName;
	}

	public String getDrugDocHtml() {
		return drugDocHtml;
	}

	public void setDrugDocHtml(String drugDocHtml) {
		this.drugDocHtml = drugDocHtml;
	}


}
