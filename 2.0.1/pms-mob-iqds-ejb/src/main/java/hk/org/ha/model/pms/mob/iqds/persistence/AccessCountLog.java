package hk.org.ha.model.pms.mob.iqds.persistence;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;

@Entity
@Table(name = "ACCESS_COUNT_LOG")
@IdClass(AccessCountLogPK.class)
public class AccessCountLog {
	
	@Id
	@Converter(name = "AccessCountLog.type", converterClass = AccessCountLogType.Converter.class)
    @Convert("AccessCountLog.type")
	@Column(name="TYPE", nullable = false, length = 2)
	private AccessCountLogType type;

	@Id
	@Column(name = "NAME", nullable = false)
	private String name;

	@Id
	@Column(name = "LOG_DATE", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date logDate;
	
	@Id
	@Converter(name = "AccessCountLog.deviceType", converterClass = DeviceType.Converter.class)
    @Convert("AccessCountLog.deviceType")
	@Column(name="DEVICE_TYPE", nullable = false, length = 1)
	private DeviceType deviceType;
	
	@Column(name = "RECORD_COUNT", nullable = false)
	private Integer recordCount;

	public AccessCountLogType getType() {
		return type;
	}

	public void setType(AccessCountLogType type) {
		this.type = type;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Date getLogDate() {
		return logDate;
	}

	public void setLogDate(Date logDate) {
		this.logDate = logDate;
	}

	public DeviceType getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(DeviceType deviceType) {
		this.deviceType = deviceType;
	}

	public Integer getRecordCount() {
		return recordCount;
	}

	public void setRecordCount(Integer recordCount) {
		this.recordCount = recordCount;
	}

}
