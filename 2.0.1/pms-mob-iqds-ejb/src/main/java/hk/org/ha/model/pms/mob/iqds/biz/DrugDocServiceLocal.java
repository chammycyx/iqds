package hk.org.ha.model.pms.mob.iqds.biz;
import hk.org.ha.model.pms.mob.iqds.vo.DrugDoc;
import hk.org.ha.model.pms.mob.iqds.vo.ReturnResult;

import javax.ejb.Local;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

@Local
@Path("/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface DrugDocServiceLocal {

	@GET
	@Path("drugDoc/{itemCode}")
	DrugDoc retrieveDrugDoc(
							@Context HttpServletRequest req,				
							@HeaderParam("lang") @DefaultValue("EN") String lang,
							@HeaderParam("deviceType") @DefaultValue("I") String deviceType,
							@PathParam("itemCode") String itemCode);
	
	@POST
	@Path("drugDocs")
	ReturnResult addDrugDoc(
							@Context HttpServletRequest req,	
							hk.org.ha.model.pms.mob.iqds.persistence.DrugDoc drugDoc);
	
	@PUT
	@Path("drugDocs")
	ReturnResult updateDrugDoc(
								@Context HttpServletRequest req,	
								hk.org.ha.model.pms.mob.iqds.persistence.DrugDoc drugDoc);
	
	@DELETE
	@Path("drugDoc/{drugName}")
	ReturnResult deleteDrugDoc(
								@Context HttpServletRequest req,	
								@PathParam("drugName") String drugName);
	
}
