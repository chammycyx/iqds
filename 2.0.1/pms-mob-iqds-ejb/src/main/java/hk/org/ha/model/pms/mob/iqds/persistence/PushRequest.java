package hk.org.ha.model.pms.mob.iqds.persistence;

import hk.org.ha.fmk.pms.entity.CreateDate;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;

@Entity
@Table(name = "PUSH_REQUEST")
public class PushRequest {

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pushRequestSeq")
	@SequenceGenerator(name = "pushRequestSeq", sequenceName = "SQ_PUSH_REQUEST", initialValue = 100000000)
	private Long id;
	
	@ManyToOne(fetch=FetchType.LAZY)
   	@JoinColumn(name = "DEVICE_ID", nullable = false)
	private Device device;
    
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumns({
		@JoinColumn(name = "HOSP_CODE", referencedColumnName = "HOSP_CODE", nullable = false),
		@JoinColumn(name = "WORKSTORE_CODE", referencedColumnName = "WORKSTORE_CODE", nullable = false)
	})
	private Workstore workstore;

	@Column(name = "TICKET_DATE", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date ticketDate;
	
	@Column(name = "TICKET_NUM", nullable = false)
	private Integer ticketNum;
	
	@Converter(name = "PushRequest.status", converterClass = RecordStatus.Converter.class)
    @Convert("PushRequest.status")
	@Column(name = "STATUS", nullable = false, length = 1)
	private RecordStatus status;    
	
	@ManyToOne(fetch=FetchType.LAZY)
   	@JoinColumn(name = "TICKET_ID")
	private Ticket ticket;	
	
    @CreateDate
	@Column(name = "CREATE_DATE", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;
    
	@Column(name = "COMPLETE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date completeDate;
	
    public PushRequest() {
		super();
		this.status = RecordStatus.Active;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getCompleteDate() {
		return completeDate;
	}

	public void setCompleteDate(Date completeDate) {
		this.completeDate = completeDate;
	}

	public Device getDevice() {
		return device;
	}

	public void setDevice(Device device) {
		this.device = device;
	}

	public Workstore getWorkstore() {
		return workstore;
	}

	public void setWorkstore(Workstore workstore) {
		this.workstore = workstore;
	}

	public Date getTicketDate() {
		return ticketDate;
	}

	public void setTicketDate(Date ticketDate) {
		this.ticketDate = ticketDate;
	}

	public Integer getTicketNum() {
		return ticketNum;
	}

	public void setTicketNum(Integer ticketNum) {
		this.ticketNum = ticketNum;
	}

	public RecordStatus getStatus() {
		return status;
	}

	public void setStatus(RecordStatus status) {
		this.status = status;
	}
	
	public Ticket getTicket() {
		return ticket;
	}

	public void setTicket(Ticket ticket) {
		this.ticket = ticket;
	}	
}
