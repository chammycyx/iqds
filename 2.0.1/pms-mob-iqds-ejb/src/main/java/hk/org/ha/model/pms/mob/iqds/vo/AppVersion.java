package hk.org.ha.model.pms.mob.iqds.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AppVersion {

	private String compatibleVersion;
	
	private String latestVersion;

	public AppVersion(String compatibleVersion, String latestVersion) {
		super();
		this.compatibleVersion = compatibleVersion;
		this.latestVersion = latestVersion;
	}

	public String getCompatibleVersion() {
		return compatibleVersion;
	}
	
	public void setCompatibleVersion(String compatibleVersion) {
		this.compatibleVersion = compatibleVersion;
	}

	public String getLatestVersion() {
		return latestVersion;
	}

	public void setLatestVersion(String latestVersion) {
		this.latestVersion = latestVersion;
	}	
}
