package hk.org.ha.model.pms.mob.iqds.biz.reftable;
import hk.org.ha.model.pms.mob.iqds.persistence.InfoLink;

import java.util.Collection;

import javax.ejb.Local;

@Local
public interface InfoLinkManagerLocal {

	Collection<InfoLink> retrieveInfoLinks();	

}
