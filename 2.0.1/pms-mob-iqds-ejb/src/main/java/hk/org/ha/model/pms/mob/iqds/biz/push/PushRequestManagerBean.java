package hk.org.ha.model.pms.mob.iqds.biz.push;

import hk.org.ha.fmk.pms.util.Pair;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.mob.iqds.persistence.Device;
import hk.org.ha.model.pms.mob.iqds.persistence.PushRequest;
import hk.org.ha.model.pms.mob.iqds.persistence.RecordStatus;
import hk.org.ha.model.pms.mob.iqds.persistence.Ticket;
import hk.org.ha.model.pms.mob.iqds.persistence.Workstore;
import hk.org.ha.model.pms.mob.iqds.vo.ReturnResult;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("pushRequestManager")
@MeasureCalls
public class PushRequestManagerBean implements PushRequestManagerLocal {
	
	@PersistenceContext
	private EntityManager em;	
	
	@SuppressWarnings("unchecked")
	public PushRequest retrievePushRequest(Workstore workstore, Date ticketDate, Integer ticketNum, Device device) 
	{
		List<PushRequest> list = em.createQuery(
				"select o from PushRequest o" + // 20131023 index check : PushRequest.workstore,ticketDate,ticketNum : I_PUSH_REQUEST_02
				" where o.workstore = :workstore" +
				" and o.ticketDate = :ticketDate" + 
				" and o.ticketNum = :ticketNum" +
				" and o.device = :device")
				.setParameter("workstore", workstore)
				.setParameter("ticketDate", ticketDate)
				.setParameter("ticketNum", ticketNum)
				.setParameter("device", device)
				.setHint(QueryHints.FETCH, "o.device")
				.setHint(QueryHints.FETCH, "o.workstore")
				.getResultList();
		
		return list.isEmpty() ? null : list.get(0);
	}
	
	public ReturnResult savePushRequest(Workstore workstore, Date ticketDate, Integer ticketNum, Device device) 
	{
		PushRequest pushRequest = retrievePushRequest(workstore, ticketDate, ticketNum, device);

		if (pushRequest == null) {
			
			pushRequest = new PushRequest();
			pushRequest.setWorkstore(workstore);
			pushRequest.setTicketDate(ticketDate);
			pushRequest.setTicketNum(ticketNum);
			pushRequest.setDevice(device);			

			em.persist(pushRequest);
			return new ReturnResult(1, "successfully created");
			
		} else if (pushRequest.getStatus() != RecordStatus.Active) {
			
			pushRequest.setStatus(RecordStatus.Active);
			
			em.flush();							
			return new ReturnResult(2, "successfully updated");
		}
		
		return new ReturnResult(-1, "already exist");
	}

	public ReturnResult deletePushRequest(Device device, Long pushRequestId) 
	{		
		PushRequest pushRequest = em.find(PushRequest.class, pushRequestId);
		
		if (pushRequest == null) {
			return new ReturnResult(-1, "not exist");
		}
		
		if (!pushRequest.getDevice().isMatch(device)) {
			return new ReturnResult(-3, "unexpected error");
		}
		
		switch (pushRequest.getStatus()) {
			case Active : 
				pushRequest.setStatus(RecordStatus.Withdraw);
				em.flush();							
				return new ReturnResult(1, "successfully withdrawed");
			case Complete : 
				pushRequest.setStatus(RecordStatus.Delete);
				em.flush();							
				return new ReturnResult(2, "successfully deleted");
			default :
				return new ReturnResult(-2, "already deleted");
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<PushRequest> retrievePushRequests(Device device, Date ticketDate) 
	{
		return (List<PushRequest>) em.createQuery(
				"select o from PushRequest o" + // 20131023 index check : PushRequest.device,ticketDate : I_PUSH_REQUEST_01
				" where o.device = :device" +
				" and o.status in :status" + 
				" and o.ticketDate = :ticketDate" + 
				" order by o.createDate")
				.setParameter("device", device)
				.setParameter("status", Arrays.asList(RecordStatus.Active, RecordStatus.Complete))
				.setParameter("ticketDate", ticketDate)
				.setHint(QueryHints.BATCH, "o.workstore")
				.setHint(QueryHints.BATCH, "o.ticket")
				.getResultList();		
	}
	
	@SuppressWarnings("unchecked")
	public List<PushRequest> retrievePushRequests(Workstore workstore, Date ticketDate, Integer ticketNum) 
	{
		return (List<PushRequest>) em.createQuery(
				"select o from PushRequest o" + // 20131023 index check : PushRequest.workstore,ticketDate,ticketNum : I_PUSH_REQUEST_02
				" where o.workstore = :workstore" +
				" and o.ticketDate = :ticketDate" + 
				" and o.ticketNum = :ticketNum" +
				" and o.status = :status" + 
				" order by o.createDate")
				.setParameter("workstore", workstore)
				.setParameter("ticketDate", ticketDate)
				.setParameter("ticketNum", ticketNum)
				.setParameter("status", RecordStatus.Active)
				.setHint(QueryHints.FETCH, "o.device")
				.getResultList();		
	}
	
	@SuppressWarnings("unchecked")
	public Map<Long, Integer> retrieveUncollectCount(List<Long> deviceIds, Date ticketDate) 
	{
		List<Pair<Long, Long>> list = (List<Pair<Long, Long>>) em.createQuery(
				"select new hk.org.ha.fmk.pms.util.Pair(o.device.id,count(o)) from PushRequest o" + // 20131023 index check : PushRequest.device,ticketDate : I_PUSH_REQUEST_01
				" where o.device.id in :deviceIds" +
				" and o.ticket.ticketDate = :ticketDate" + 
				" and o.ticket.collectDate is null" + 
				" and o.status = :status" + 
				" group by o.device.id")
				.setParameter("deviceIds", deviceIds)
				.setParameter("ticketDate", ticketDate)
				.setParameter("status", RecordStatus.Complete)
				.getResultList();

		Map<Long, Integer> ret = new HashMap<Long, Integer>();
		for (Pair<Long, Long> pair : list) {
			ret.put(pair.getFirst(), Integer.valueOf(pair.getSecond().intValue()));
		}
		return ret;
	}
	
	@SuppressWarnings("unchecked")
	public void unlinkPushRequests(Ticket ticket) {
		
		List<PushRequest> list = (List<PushRequest>) em.createQuery(
				"select o from PushRequest o" + // 20131206 index check : PushRequest.ticket : FK_PUSH_REQUEST_02
				" where o.ticket = :ticket" + 
				" order by o.createDate")
				.setParameter("ticket", ticket)
				.getResultList();	
		
		for (PushRequest pushRequest : list) {			
			if (RecordStatus.Complete == pushRequest.getStatus()) {
				pushRequest.setStatus(RecordStatus.Active);
				pushRequest.setCompleteDate(null);
			}
			pushRequest.setTicket(null);
		}
		
		em.flush();
	}
	
}
