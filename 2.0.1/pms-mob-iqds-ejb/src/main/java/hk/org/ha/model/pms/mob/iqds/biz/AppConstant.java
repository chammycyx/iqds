package hk.org.ha.model.pms.mob.iqds.biz;

import java.util.Arrays;
import java.util.List;

public final class AppConstant {

	private AppConstant() {
	}

	public static final String APPLICATION_PROPERTIES = "application.properties";
	
	public static final String SECTION_PROPERTIES = "drugDocSectionId.properties";
	
	public static final String SECTION_ZHTW_PROPERTIES = "drugDocSectionId_zh_TW.properties";

	public static final String HEADER_PROPERTIES = "header.properties";
	
	public static final String HEADER_ZHTW_PROPERTIES = "header_zh_TW.properties";
	
	public static final String FOOTER_PROPERTIES = "footer.properties";
	
	public static final String FOOTER_ZHTW_PROPERTIES = "footer_zh_TW.properties";
	
	public static final String DISP_BOARD_RELOAD_INTERVAL = "disp-board.reload-interval";
	
	public static final String TICKET_UPDATE_TASK_INTERVAL = "ticket.update-task-interval";
	
	public static final String ALLOWED_IP_ADDRESSES = "allowed-ip-addresses";
		
	public static final String DISP_BOARD_REGION = "DispBoard";
	
	public static final String TICKET = "ticket";
	
	public static final String DRUG_DOC = "drug-doc";
	
	public static final String DRUG_DOC_DISPLAY_SECTION_ID_LIST= "drug-doc.display.section-id-list";
	
	public static final Integer TIMEOUT_WINDOW_NUM = Integer.valueOf(0);
	
	public static final Integer SINGLE_QUEUE_WINDOW_NUM = Integer.valueOf(16);
	
	public static final List<Integer> ENQUIRY_WINDOW_NUM_LIST = Arrays.asList(
			Integer.valueOf(17),
			Integer.valueOf(18),
			Integer.valueOf(19),
			Integer.valueOf(20)
	);
	
	public static final List<Integer> DEFAULT_WINDOW_NUM_LIST = Arrays.asList(
			Integer.valueOf(1),
			Integer.valueOf(2),
			Integer.valueOf(3),
			Integer.valueOf(4),
			Integer.valueOf(5),
			Integer.valueOf(6),
			Integer.valueOf(7),
			Integer.valueOf(8),
			Integer.valueOf(9),
			Integer.valueOf(10),
			Integer.valueOf(11),
			Integer.valueOf(12),
			Integer.valueOf(13),
			Integer.valueOf(14),
			Integer.valueOf(15),
			Integer.valueOf(16),
			Integer.valueOf(17),
			Integer.valueOf(18),
			Integer.valueOf(19),
			Integer.valueOf(20)		
	);
}
