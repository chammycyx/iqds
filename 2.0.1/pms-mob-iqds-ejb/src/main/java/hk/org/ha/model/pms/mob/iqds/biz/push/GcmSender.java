package hk.org.ha.model.pms.mob.iqds.biz.push;

import hk.org.ha.fmk.pms.web.MeasureCalls;

import java.io.IOException;
import java.util.List;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Startup;
import org.jboss.seam.log.Log;

import com.google.android.gcm.server.Constants;
import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;

@Startup
@Name("gcmSender")
@Scope(ScopeType.APPLICATION)
@MeasureCalls
public class GcmSender {

	@Logger
	private Log logger;

	private boolean enable = false;

	private String apiKey;

	private Sender sender;

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	@Create
	public void init() {
		if (enable) {
			sender = new Sender(apiKey);
		}
	}
    
	public void sendNotifications(String message, int badge, List<String> devices) {
		
		if (!enable) {
			logger.warn("GsmSender is disabled, please check your configuration.");
			return;
		}		
						
		Message msg = new Message.Builder()
				.addData("message", message)
				.addData("badge", String.valueOf(badge)).build();
		
		Result result;
		for (String regId : devices) {		
			try {
				result = sender.send(msg, regId, 5);
			} catch (IOException e) {
				logger.error("Error posting messages.", e);
				return;
			}
	
			String messageId = result.getMessageId();
			if (messageId != null) {
				logger.debug("Succesfully sent message to device: " + regId
						+ "; messageId = " + messageId);
				String canonicalRegId = result.getCanonicalRegistrationId();
				if (canonicalRegId != null) {
					// same device has more than on registration id: update it
					logger.info("canonicalRegId " + canonicalRegId);
					//Datastore.updateRegistration(regId, canonicalRegId);
				}
			} else {
				String error = result.getErrorCodeName();
				if (error.equals(Constants.ERROR_NOT_REGISTERED)) {
					// application has been removed from device - unregister it
					logger.info("Unregistered device: " + regId);
					//Datastore.unregister(regId);
				} else {
					logger.error("Error sending message to #0: #1", regId, error);
				}
			}
		}
	}
}
