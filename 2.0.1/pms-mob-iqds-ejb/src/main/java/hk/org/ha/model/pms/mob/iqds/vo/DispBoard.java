package hk.org.ha.model.pms.mob.iqds.vo;

import java.io.Serializable;
import java.util.Collection;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DispBoard implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String hospCode;

	private String workstoreCode;
	
	private Integer reloadInterval;
		
	private Collection<DispWindow> dispWindows;
	
	public DispBoard() {
		super();
	}

	public DispBoard(String hospCode, String workstoreCode, Collection<DispWindow> dispWindows, Integer reloadInterval) {
		super();
		this.hospCode = hospCode;
		this.workstoreCode = workstoreCode;
		this.dispWindows = dispWindows;
		this.reloadInterval = reloadInterval;
	}
	
	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public String getWorkstoreCode() {
		return workstoreCode;
	}

	public void setWorkstoreCode(String workstoreCode) {
		this.workstoreCode = workstoreCode;
	}

	public Integer getReloadInterval() {
		return reloadInterval;
	}

	public void setReloadInterval(Integer reloadInterval) {
		this.reloadInterval = reloadInterval;
	}

	public Collection<DispWindow> getDispWindows() {
		return dispWindows;
	}

	public void setDispWindows(Collection<DispWindow> dispWindows) {
		this.dispWindows = dispWindows;
	}
}
