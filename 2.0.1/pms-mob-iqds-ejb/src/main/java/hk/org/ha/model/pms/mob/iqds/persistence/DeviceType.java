package hk.org.ha.model.pms.mob.iqds.persistence;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum DeviceType implements StringValuedEnum {

	iOS("I", "iOS"),
	Android("A", "Android");
	
    private final String dataValue;
    private final String displayValue;

    DeviceType(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static DeviceType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(DeviceType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<DeviceType> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<DeviceType> getEnumClass() {
    		return DeviceType.class;
    	}
    }
}



