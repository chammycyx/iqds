package hk.org.ha.model.pms.mob.iqds.biz.push;
import hk.org.ha.model.pms.mob.iqds.persistence.Device;
import hk.org.ha.model.pms.mob.iqds.persistence.PushRequest;
import hk.org.ha.model.pms.mob.iqds.persistence.Ticket;
import hk.org.ha.model.pms.mob.iqds.persistence.Workstore;
import hk.org.ha.model.pms.mob.iqds.vo.ReturnResult;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

@Local
public interface PushRequestManagerLocal {
	
	PushRequest retrievePushRequest(
			Workstore workstore, 
			Date ticketDate, 
			Integer ticketNum, 
			Device device);

	ReturnResult savePushRequest(
			Workstore workstore, 
			Date ticketDate,
			Integer ticketNum, 
			Device device);

	ReturnResult deletePushRequest(
			Device device, Long pushRequestId);

	List<PushRequest> retrievePushRequests(
			Device device,
			Date ticketDate);

	List<PushRequest> retrievePushRequests(
			Workstore workstore, 
			Date ticketDate, 
			Integer ticketNum);
	
	Map<Long, Integer> retrieveUncollectCount(
			List<Long> deviceIds, 
			Date ticketDate);
	
	void unlinkPushRequests(Ticket ticket);
	
}
