package hk.org.ha.model.pms.mob.iqds.biz;
import javax.ejb.Local;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

@Local
@Path("/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface TicketServiceLocal {

	@PUT
	@Path("ticket/{ticketNum}/issue")
	void issueTicket(
			@Context HttpServletRequest req,
			@HeaderParam("HospCode") String hospCode, 
			@HeaderParam("WorkstoreCode") String workstoreCode,			
			@PathParam("ticketNum") Integer ticketNum,
			@QueryParam("issueWindowNum") Integer issueWindowNum);

	@PUT
	@Path("ticket/{ticketNum}/collect")
	void collectTicket(
			@Context HttpServletRequest req,
			@HeaderParam("HospCode") String hospCode, 
			@HeaderParam("WorkstoreCode") String workstoreCode,			
			@PathParam("ticketNum") Integer ticketNum);
}
