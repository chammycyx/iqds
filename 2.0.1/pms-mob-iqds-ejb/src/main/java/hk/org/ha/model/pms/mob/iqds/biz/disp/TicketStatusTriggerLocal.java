package hk.org.ha.model.pms.mob.iqds.biz.disp;

import javax.ejb.Local;
import org.jboss.seam.annotations.async.Asynchronous;
import org.jboss.seam.annotations.async.Duration;
import org.jboss.seam.annotations.async.IntervalDuration;

@Local
public interface TicketStatusTriggerLocal {
	@Asynchronous
	void processIssueTicketByWorkstore(String hospCode, String workstoreCode);
	
	@Asynchronous
	void processIssueTicketsByTimeout(@Duration Long duration, @IntervalDuration Long intervalDuration);
}
