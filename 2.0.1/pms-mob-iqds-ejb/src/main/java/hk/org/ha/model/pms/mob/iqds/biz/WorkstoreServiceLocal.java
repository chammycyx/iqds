package hk.org.ha.model.pms.mob.iqds.biz;
import hk.org.ha.model.pms.mob.iqds.persistence.Workstore;

import java.util.Collection;

import javax.ejb.Local;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Local
@Path("/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface WorkstoreServiceLocal {

	@GET
	@Path("workstores")
	Collection<Workstore> retrieveWorkstores();

}
