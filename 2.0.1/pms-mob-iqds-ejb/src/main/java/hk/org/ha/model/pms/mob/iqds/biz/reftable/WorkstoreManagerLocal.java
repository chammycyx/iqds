package hk.org.ha.model.pms.mob.iqds.biz.reftable;

import hk.org.ha.model.pms.mob.iqds.persistence.Workstore;

import java.util.Collection;
import java.util.List;

import javax.ejb.Local;

@Local
public interface WorkstoreManagerLocal {

	Collection<Workstore> retrieveWorkstores();	
	
	List<Workstore> retrieveWorkstoresWithWindowTicketCount();
	
	Workstore retrieveWorkstore(String hospCode, String workstoreCode);
}
