package hk.org.ha.model.pms.mob.iqds.biz;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.mob.iqds.biz.disp.TicketManagerLocal;
import hk.org.ha.model.pms.mob.iqds.biz.reftable.WorkstoreManagerLocal;
import hk.org.ha.model.pms.mob.iqds.persistence.Workstore;
import hk.org.ha.service.biz.pms.iqds.interfaces.IqdsTicketProcessorJmsRemote;

import java.util.Date;

import javax.ejb.Stateless;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.joda.time.DateMidnight;

@AutoCreate
@Stateless
@Name("ticketProcessService")
@MeasureCalls
public class TicketProcessServiceBean implements TicketProcessServiceLocal {

	@In
	private WorkstoreManagerLocal workstoreManager;

	@In
	private TicketManagerLocal ticketManager;
	
	@In
	private IqdsTicketProcessorJmsRemote iqdsTicketProcessorProxy;
			
	public void issueTicket(String hospCode, String workstoreCode, Integer ticketNum, Integer issueWindowNum)
	{
		Workstore workstore = workstoreManager.retrieveWorkstore(hospCode, workstoreCode);
		Date ticketDate = new DateMidnight().toDate();
		
		ticketManager.issueTicket(workstore, ticketDate, ticketNum, issueWindowNum);
	}

	public void collectTicket(String hospCode, String workstoreCode, Integer ticketNum) 
	{
		Workstore workstore = workstoreManager.retrieveWorkstore(hospCode, workstoreCode);
		Date ticketDate = new DateMidnight().toDate();
		
		boolean processPendingTicket = ticketManager.collectTicket(workstore,  ticketDate, ticketNum);

		if( processPendingTicket ){
			iqdsTicketProcessorProxy.processTicket(workstore, ticketDate, ticketNum);
		}
	}
}
