package hk.org.ha.model.pms.mob.iqds.vo;

import java.io.Serializable;
import java.util.Date;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Ticket implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	
	private Integer ticketNum;
	
	private Date issueDate;
	
	public Ticket() {
		super();
	}
	
	public Ticket(Long id, Integer ticketNum, Date issueDate) {
		super();
		this.id = id;
		this.ticketNum = ticketNum;
		this.issueDate = issueDate;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getTicketNum() {
		return ticketNum;
	}

	public void setTicketNum(Integer ticketNum) {
		this.ticketNum = ticketNum;
	}

	public Date getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}
}
