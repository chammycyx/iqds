package hk.org.ha.model.pms.mob.iqds.biz;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.mob.iqds.biz.disp.TicketStatusTriggerLocal;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.servlet.http.HttpServletRequest;

import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

@Stateless
@Name("ticketService")
@MeasureCalls
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class TicketServiceBean implements TicketServiceLocal {

	@In
	private TicketProcessServiceLocal ticketProcessService;
		
	@In
	private TicketStatusTriggerLocal ticketStatusTrigger;
	
	@In
	private AllowedIpAddressesChecker allowedIpAddressesChecker;
			
	public void issueTicket(HttpServletRequest req, String hospCode, String workstoreCode, Integer ticketNum, Integer issueWindowNum) 	
	{	
		allowedIpAddressesChecker.checkAccess(AppConstant.TICKET, req.getRemoteAddr());
		
		ticketProcessService.issueTicket(hospCode, workstoreCode, ticketNum, issueWindowNum);
		
		ticketStatusTrigger.processIssueTicketByWorkstore(hospCode, workstoreCode);
	}

	public void collectTicket(HttpServletRequest req, String hospCode, String workstoreCode, Integer ticketNum) 
	{
		allowedIpAddressesChecker.checkAccess(AppConstant.TICKET, req.getRemoteAddr());
		
		ticketProcessService.collectTicket(hospCode, workstoreCode, ticketNum);
		
		ticketStatusTrigger.processIssueTicketByWorkstore(hospCode, workstoreCode);
	}
	

}

