package hk.org.ha.model.pms.mob.iqds.json;

import java.text.SimpleDateFormat;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

import org.codehaus.jackson.map.ObjectMapper;

@Provider
@Produces(MediaType.APPLICATION_JSON)
public class JacksonContextResolver implements ContextResolver<ObjectMapper> {

	public static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";

	private ObjectMapper objectMapper;

	public JacksonContextResolver() throws Exception {
        objectMapper = new ObjectMapper();
        objectMapper.setDateFormat(new SimpleDateFormat(DATE_FORMAT));

        // cannot use since the timezone always output as +0000 
        // i.e. 2013-11-13T08:53:08.241+0000 instead of 2013-11-13T16:53:08.241+0800
        // objectMapper.configure(SerializationConfig.Feature.WRITE_DATES_AS_TIMESTAMPS, false);
    }
	
	@Override
	public ObjectMapper getContext(Class<?> arg0) {
		return objectMapper;
	}
}
