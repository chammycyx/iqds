package hk.org.ha.model.pms.mob.iqds.biz.reftable;

import hk.org.ha.fmk.pms.cache.CacheResult;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.mob.iqds.persistence.InfoLink;

import java.util.Collection;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("infoLinkManager")
@MeasureCalls
public class InfoLinkManagerBean implements InfoLinkManagerLocal {

	@PersistenceContext
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	@CacheResult(timeout="60000")
	public Collection<InfoLink> retrieveInfoLinks() 
	{
		return (Collection<InfoLink>) em.createQuery(
				"select o from InfoLink o" + // 20121023 index check : none
				" order by o.sortSeq")
				.getResultList();
	}		
}
