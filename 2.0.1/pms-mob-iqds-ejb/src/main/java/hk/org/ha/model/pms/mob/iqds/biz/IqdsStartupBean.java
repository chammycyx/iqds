package hk.org.ha.model.pms.mob.iqds.biz;

import hk.org.ha.model.pms.mob.iqds.biz.disp.TicketStatusTriggerLocal;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("iqdsStartup")
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class IqdsStartupBean implements IqdsStartupLocal {

	private static final long ONE_MINUTE_MILLI = 60000L;
	private static final long UPDATE_TASK_INTERVAL = ONE_MINUTE_MILLI;
	
	@In
	private TicketStatusTriggerLocal ticketStatusTrigger;
	
	@Override
	public void startup(Long delayDuration) {
		startupTriggerTicketStatus();
	}
	
	private void startupTriggerTicketStatus(){
		ticketStatusTrigger.processIssueTicketsByTimeout(ONE_MINUTE_MILLI, UPDATE_TASK_INTERVAL);
	}
}
