package hk.org.ha.model.pms.mob.iqds.biz.doc;

import hk.org.ha.model.pms.mob.iqds.persistence.DrugDoc;
import hk.org.ha.model.pms.mob.iqds.vo.ReturnResult;

import javax.ejb.Local;

@Local
public interface DrugDocManagerLocal {
	
	DrugDoc retrieveDrugDoc(String itemCode);
	
	ReturnResult addDrugDoc(DrugDoc drugDoc);
	
	ReturnResult updateDrugDoc(DrugDoc drugDoc);
	
	ReturnResult deleteDrugDoc(String drugName);
}
