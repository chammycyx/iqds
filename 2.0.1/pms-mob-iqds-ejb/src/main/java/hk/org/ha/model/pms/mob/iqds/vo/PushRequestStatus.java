package hk.org.ha.model.pms.mob.iqds.vo;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum PushRequestStatus implements StringValuedEnum {

	Enquiry("Enquiry", "Enquiry", 1, 1),	
	Ready("Ready", "Ready", 2, 3),	
	Pending("Pending", "Pending", 3, 2),
	Taken("Taken", "Taken", 4, 4);
	
    private final String dataValue;
    private final String displayValue;
    private final int sortSeq;
    private final int sortSeqNew;
        
    PushRequestStatus(final String dataValue, final String displayValue, int sortSeq, int sortSeqNew){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
        this.sortSeq = sortSeq;
        this.sortSeqNew = sortSeqNew;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
    
    public int getSortSeq() {
    	return this.sortSeq;
    }
    
    public int getSortSeqNew() {
    	return this.sortSeqNew;
    }
    
    public static PushRequestStatus dataValueOf(String dataValue) {
    	return StringValuedEnumReflect.getObjectFromValue(PushRequestStatus.class, dataValue);
    }
        
	public static class Converter extends StringValuedEnumConverter<PushRequestStatus> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<PushRequestStatus> getEnumClass() {
    		return PushRequestStatus.class;
    	}
    }
}
