package hk.org.ha.model.pms.mob.iqds.biz;

import hk.org.ha.service.biz.pms.iqds.interfaces.IqdsTicketProcessorJmsRemote;

import javax.ejb.Local;

@Local
public interface IqdsTicketProcessorLocal extends IqdsTicketProcessorJmsRemote {
	
}
