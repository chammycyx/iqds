package hk.org.ha.model.pms.mob.iqds.biz;

import javax.ejb.Local;

@Local
public interface TicketProcessServiceLocal {
	
	void issueTicket(String hospCode, String workstoreCode, Integer ticketNum, Integer issueWindowNum);
	
	void collectTicket(String hospCode, String workstoreCode, Integer ticketNum);
}
