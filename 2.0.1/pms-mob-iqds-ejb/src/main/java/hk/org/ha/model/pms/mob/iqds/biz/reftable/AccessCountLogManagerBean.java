package hk.org.ha.model.pms.mob.iqds.biz.reftable;

import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.mob.iqds.persistence.AccessCountLog;
import hk.org.ha.model.pms.mob.iqds.persistence.DeviceType;
import hk.org.ha.model.pms.mob.iqds.persistence.AccessCountLogType;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.PessimisticLock;
import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("accessCountLogManager")
@MeasureCalls
public class AccessCountLogManagerBean implements AccessCountLogManagerLocal {

	@PersistenceContext
	private EntityManager em;
	
	@In
	private AuditLogger auditLogger;
	
	private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void countRetrieveDrugDoc(String itemCode, DeviceType deviceType,
			Date invokeDate) {
		
		AccessCountLog accessCount = null;
		try{
			accessCount = retrieveAndLockAccessCount(deviceType, invokeDate, AccessCountLogType.DrugItem, itemCode);
			accessCount.setRecordCount(accessCount.getRecordCount()+1);
		}catch(NoResultException ne){
			try{
				accessCount = new AccessCountLog();
				accessCount.setLogDate(invokeDate);
				accessCount.setRecordCount(Integer.valueOf(1));
				accessCount.setDeviceType(deviceType);
				accessCount.setName(itemCode);
				accessCount.setType(AccessCountLogType.DrugItem);
				em.persist(accessCount);
			}catch(Exception e2){
				auditLogger.log("TouchMed|My Medication|Discarded Stat|ItemCode[#0]|Date[#1]|DeviceType[#2]|DiscardReason[#3]",itemCode, dateFormat.format(invokeDate), deviceType.getDataValue(), e2.getMessage());
			}
		}catch (Exception e){
			auditLogger.log("TouchMed|My Medication|Discarded Stat|ItemCode[#0]|Date[#1]|DeviceType[#2]|DiscardReason[#3]",itemCode, dateFormat.format(invokeDate), deviceType.getDataValue(), e.getMessage());
		}
	}

	private AccessCountLog retrieveAndLockAccessCount(DeviceType deviceType, Date logDate, AccessCountLogType type, String nameValue) 
	{
		AccessCountLog accessCount = (AccessCountLog) em.createQuery(
				"select o from AccessCountLog o" + 
				" where o.deviceType = :deviceType" +
				" and o.logDate = :logDate" +
				" and o.type = :type" +
				" and o.name = :name")
				.setParameter("deviceType", deviceType)
				.setParameter("logDate", logDate)
				.setParameter("type", type)
				.setParameter("name", nameValue)
				.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock)
				.getSingleResult();
		
		return accessCount;
	}
}
