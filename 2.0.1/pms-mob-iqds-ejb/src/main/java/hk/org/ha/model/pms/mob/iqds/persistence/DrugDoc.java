package hk.org.ha.model.pms.mob.iqds.persistence;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.PrivateOwned;

@Entity
@Table(name = "DRUG_DOC")
public class DrugDoc {

	@Id
	@Column(name = "DRUG_NAME")
	private String drugName;

	@Lob
	@Column(name = "DRUG_XML")
	private String drugXml;
	
	@Column(name = "RELEASE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date releaseDate;
	
	@PrivateOwned
	@OneToMany(mappedBy="drugDoc", cascade=CascadeType.ALL)
	private List<DrugDocMapping> drugDocMappingList;


	public String getDrugName() {
		return drugName;
	}

	public void setDrugName(String drugName) {
		this.drugName = drugName;
	}

	public String getDrugXml() {
		return drugXml;
	}

	public void setDrugXml(String drugXml) {
		this.drugXml = drugXml;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public List<DrugDocMapping> getDrugDocMappingList() {
		return drugDocMappingList;
	}

	public void setDrugDocMappingList(
			List<DrugDocMapping> drugDocMappingList) {
		this.drugDocMappingList = drugDocMappingList;
	}
	
}
