package hk.org.ha.model.pms.mob.iqds.biz;
import hk.org.ha.model.pms.mob.iqds.vo.DispBoard;

import javax.ejb.Local;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Local
@Path("/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface DispBoardServiceLocal {
	
	@GET
	@Path("dispBoard/{hospCode}/{workstoreCode}")
	DispBoard retrieveDispBoard(
			@PathParam("hospCode") String hospCode, 
			@PathParam("workstoreCode") String workstoreCode);			
	
}
