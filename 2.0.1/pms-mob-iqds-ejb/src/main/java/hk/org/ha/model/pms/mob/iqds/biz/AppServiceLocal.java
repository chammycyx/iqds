package hk.org.ha.model.pms.mob.iqds.biz;
import hk.org.ha.model.pms.mob.iqds.vo.AppVersion;

import javax.ejb.Local;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Local
@Path("/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface AppServiceLocal {

	@GET
	@Path("appVersion")
	AppVersion retrieveAppVersion(
			@HeaderParam("deviceType") @DefaultValue("I") String deviceType,
			@HeaderParam("deviceOSVersion") @DefaultValue("6") String deviceOSVersion);
}
