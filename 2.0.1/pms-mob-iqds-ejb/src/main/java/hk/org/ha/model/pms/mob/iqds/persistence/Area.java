package hk.org.ha.model.pms.mob.iqds.persistence;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum Area implements StringValuedEnum {

	HK("HK", "Hong Kong"),
	KLN("KLN", "Kowloon"),
	NT("NT", "New Territories");
	
    private final String dataValue;
    private final String displayValue;

    Area(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static Area dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(Area.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<Area> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<Area> getEnumClass() {
    		return Area.class;
    	}
    }
}



