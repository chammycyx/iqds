package hk.org.ha.model.pms.mob.iqds.persistence;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum Lang implements StringValuedEnum {

	EN("EN", "English"),
	TC("TC", "Tranditional Chinese");
	
    private final String dataValue;
    private final String displayValue;

    Lang(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static Lang dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(Lang.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<Lang> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<Lang> getEnumClass() {
    		return Lang.class;
    	}
    }
}



