package hk.org.ha.model.pms.mob.iqds.biz;

import hk.org.ha.fmk.pms.cache.CacheProvider;
import hk.org.ha.fmk.pms.util.PropertiesHelper;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.mob.iqds.biz.disp.TicketManagerLocal;
import hk.org.ha.model.pms.mob.iqds.biz.reftable.WorkstoreManagerLocal;
import hk.org.ha.model.pms.mob.iqds.persistence.Workstore;
import hk.org.ha.model.pms.mob.iqds.vo.DispBoard;
import hk.org.ha.model.pms.mob.iqds.vo.DispWindow;
import hk.org.ha.model.pms.mob.iqds.vo.Ticket;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;

import javax.ejb.Stateless;

import net.sf.ehcache.CacheManager;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;
import org.joda.time.DateMidnight;
import org.joda.time.DateTime;

/**
 * Session Bean implementation class DispBoardServiceBean
 */
@AutoCreate
@Stateless
@Name("dispBoardService")
@MeasureCalls
public class DispBoardServiceBean implements DispBoardServiceLocal {
	
	private static final int DEFAULT_RELOAD_INTERVAL = 10000;
			
	@Logger
	private Log logger;
	
	@In
	private WorkstoreManagerLocal workstoreManager;

	@In
	private TicketManagerLocal ticketManager;
	
	@In
	private CacheProvider<CacheManager> cacheProvider;	
	
	public DispBoard retrieveDispBoard(String hospCode, String workstoreCode)
	{
		String key = hospCode + "_" + workstoreCode;
		DispBoard dispBoard = (DispBoard) cacheProvider.get(AppConstant.DISP_BOARD_REGION, key);
		if (dispBoard == null) {
			dispBoard = buildDispBoard(hospCode, workstoreCode);
			cacheProvider.put(AppConstant.DISP_BOARD_REGION, key, dispBoard);
		}
		return dispBoard;
	}
	
	private DispBoard buildDispBoard(String hospCode, String workstoreCode)
	{
		logger.debug("buildDispBoard for Workstore #0,#1", hospCode, workstoreCode);
		
		Workstore workstore = workstoreManager.retrieveWorkstore(hospCode, workstoreCode);		

		DateTime timeoutTime = null;
		if (workstore.getTimeoutInterval().intValue() > 0) {
			timeoutTime = new DateTime().minusMinutes(workstore.getTimeoutInterval().intValue());
		}
		
		Collection<hk.org.ha.model.pms.mob.iqds.persistence.Ticket> ticketList = 
			ticketManager.retrieveTickets(workstore, new DateMidnight().toDate());
		
		Map<Integer, DispWindow> dispWindowMap = new TreeMap<Integer, DispWindow>();		

		for (hk.org.ha.model.pms.mob.iqds.persistence.Ticket ticket : ticketList) 
		{
			Integer issueWindowNum = null;
			Integer sectionNum = null;
			
			if( AppConstant.ENQUIRY_WINDOW_NUM_LIST.contains(ticket.getIssueWindowNum()) ){
				issueWindowNum = ticket.getIssueWindowNum();
				sectionNum = Integer.valueOf(2);
			}else if (timeoutTime != null && new DateTime(ticket.getIssueDate()).isBefore(timeoutTime)) {
				if (workstore.getShowTimeoutFlag().booleanValue()) {
					issueWindowNum = AppConstant.TIMEOUT_WINDOW_NUM;
					sectionNum = Integer.valueOf(3);
				}
			} else {
				issueWindowNum = ticket.getIssueWindowNum();
				if( AppConstant.SINGLE_QUEUE_WINDOW_NUM.equals(issueWindowNum) ){
					sectionNum = Integer.valueOf(0);
				}else{
					sectionNum = Integer.valueOf(1);
				}
			}
			
			if (issueWindowNum != null) {
				DispWindow dispWindow = dispWindowMap.get(issueWindowNum);
				if (dispWindow == null) {
					dispWindow = new DispWindow(workstore.getHospCode(), workstore.getWorkstoreCode(), issueWindowNum, sectionNum);
					dispWindowMap.put(issueWindowNum, dispWindow);
				}
				dispWindow.getTickets().add(new Ticket(ticket.getId(), ticket.getTicketNum(), ticket.getIssueDate()));
			}
		}
		
		int reloadInterval = DEFAULT_RELOAD_INTERVAL;
		
		try {
			reloadInterval = PropertiesHelper.getPropertyAsInt(
					AppConstant.APPLICATION_PROPERTIES, 
					AppConstant.DISP_BOARD_RELOAD_INTERVAL,
					DEFAULT_RELOAD_INTERVAL);
		} catch (IOException e) {
			logger.warn("unable to read #0 under #1", 
					AppConstant.DISP_BOARD_RELOAD_INTERVAL, 
					AppConstant.APPLICATION_PROPERTIES);
		}
		
		return new DispBoard(
				hospCode, workstoreCode, 
				new ArrayList<DispWindow>(dispWindowMap.values()), 
				Integer.valueOf(reloadInterval));
	}
	
}
