package hk.org.ha.model.pms.mob.iqds.persistence;

import hk.org.ha.fmk.pms.entity.CreateDate;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;

@Entity
@Table(name = "WORKSTORE")
@IdClass(WorkstorePK.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Workstore implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "HOSP_CODE", length = 3)
	private String hospCode;
	
	@Id
	@Column(name = "WORKSTORE_CODE", length = 4)
	private String workstoreCode;
	
	@Column(name = "NAME", nullable = false, length = 100)
	private String name;

	@Column(name = "NAME_TC", nullable = false, length = 300)
	private String nameTc;
	
	@Column(name="ADDRESS", nullable = false, length = 500)
	private String address;
	
	@Column(name="ADDRESS_TC", nullable = false, length = 1500)
	private String addressTc;

	@Column(name="CONTACT", length = 100)
	private String contact;
	
	@Column(name="SERVICE_HOUR", length = 500)
	private String serviceHour;
	
	@Column(name="SERVICE_HOUR_TC", length = 1500)
	private String serviceHourTc;

	@Column(name="REMARK", length = 1000)
	private String remark;

	@Column(name="REMARK_TC", length = 2000)
	private String remarkTc;
	
	@Column(name="IMAGE_URL", length=100)
	private String imageUrl;
	
	@Column(name="LATITUDE", nullable = false, precision = 9, scale = 6)
	private Double latitude;
	
	@Column(name="LONGITUDE", nullable = false, precision = 9, scale = 6)
	private Double longitude;

	@Converter(name = "Workstore.area", converterClass = Area.Converter.class)
    @Convert("Workstore.area")
	@Column(name = "AREA", nullable = false, length = 3)
	private Area area;
		
	@Column(name = "TIMEOUT_INTERVAL", nullable = false)
	private Integer timeoutInterval;
	
	@Column(name = "SHOW_TIMEOUT_FLAG", nullable = false)
	private Boolean showTimeoutFlag;
	
	@Converter(name = "Workstore.status", converterClass = RecordStatus.Converter.class)
    @Convert("Workstore.status")
	@Column(name = "STATUS", nullable = false, length = 1)
	private RecordStatus status;    
        
    @CreateDate
	@Column(name = "CREATE_DATE", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

    @Column(name = "WINDOW_TICKET_COUNT")
    private Integer windowTicketCount;
    
	@Column(name = "WINDOW_TICKET_TASK_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date windowTicketTaskDate;
    	
	public Workstore() {
		status = RecordStatus.Active;
		timeoutInterval = Integer.valueOf(0);
		setWindowTicketCount(Integer.valueOf(0));
		showTimeoutFlag = Boolean.TRUE;
	}
	
	public Workstore(String hospCode, String workstoreCode) {
		this();
		this.hospCode = hospCode;
		this.workstoreCode = workstoreCode;
	}
		
	public String getHospCode() {
		return hospCode;
	}
	
	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public String getWorkstoreCode() {
		return workstoreCode;
	}

	public void setWorkstoreCode(String workstoreCode) {
		this.workstoreCode = workstoreCode;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNameTc() {
		return nameTc;
	}

	public void setNameTc(String nameTc) {
		this.nameTc = nameTc;
	}

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}
	
	@JsonIgnore
	public Integer getTimeoutInterval() {
		return timeoutInterval;
	}

	public void setTimeoutInterval(Integer timeoutInterval) {
		this.timeoutInterval = timeoutInterval;
	}

	@JsonIgnore
	public Boolean getShowTimeoutFlag() {
		return showTimeoutFlag;
	}

	public void setShowTimeoutFlag(Boolean showTimeoutFlag) {
		this.showTimeoutFlag = showTimeoutFlag;
	}

	@JsonIgnore
	public RecordStatus getStatus() {
		return status;
	}

	public void setStatus(RecordStatus status) {
		this.status = status;
	}
	
	@JsonIgnore
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@JsonIgnore
	public WorkstorePK getId() {
		return new WorkstorePK(this.getHospCode(), this.getWorkstoreCode());
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAddressTc() {
		return addressTc;
	}

	public void setAddressTc(String addressTc) {
		this.addressTc = addressTc;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getServiceHour() {
		return serviceHour;
	}

	public void setServiceHour(String serviceHour) {
		this.serviceHour = serviceHour;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public String getServiceHourTc() {
		return serviceHourTc;
	}

	public void setServiceHourTc(String serviceHourTc) {
		this.serviceHourTc = serviceHourTc;
	}

	public String getRemarkTc() {
		return remarkTc;
	}

	public void setRemarkTc(String remarkTc) {
		this.remarkTc = remarkTc;
	}

	@JsonIgnore
	public Integer getWindowTicketCount() {
		return windowTicketCount;
	}
	
	public void setWindowTicketCount(Integer windowTicketCount) {
		this.windowTicketCount = windowTicketCount;
	}
	
	@JsonIgnore
	public Date getWindowTicketTaskDate() {
		return windowTicketTaskDate;
	}
	
	public void setWindowTicketTaskDate(Date windowTicketTaskDate) {
		this.windowTicketTaskDate = windowTicketTaskDate;
	}
	
	@JsonIgnore
	public boolean isBufferQueueEnabled(){
		return (getWindowTicketCount() != null && getWindowTicketCount() > 0);
	}

}
