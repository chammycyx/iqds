package hk.org.ha.model.pms.mob.iqds.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ReturnResult {

	private int code;
	private String message;
	private String extra;
	
	public ReturnResult(int code, String message) {
		this(code, message, null);
	}
	
	public ReturnResult(int code, String message, String extra) {
		super();
		this.code = code;
		this.message = message;
		this.extra = extra;
	}
	
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

	public String getExtra() {
		return extra;
	}

	public void setExtra(String extra) {
		this.extra = extra;
	}
}
