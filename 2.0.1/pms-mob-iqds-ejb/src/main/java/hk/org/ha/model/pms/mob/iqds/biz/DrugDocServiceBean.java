package hk.org.ha.model.pms.mob.iqds.biz;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.fmk.pms.util.PropertiesHelper;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.mob.iqds.biz.doc.DrugDocManagerLocal;
import hk.org.ha.model.pms.mob.iqds.biz.reftable.AccessCountLogManagerLocal;
import hk.org.ha.model.pms.mob.iqds.persistence.DeviceType;
import hk.org.ha.model.pms.mob.iqds.persistence.Lang;
import hk.org.ha.model.pms.mob.iqds.vo.DrugDoc;
import hk.org.ha.model.pms.mob.iqds.vo.DrugDocument;
import hk.org.ha.model.pms.mob.iqds.vo.ReturnResult;
import hk.org.ha.model.pms.mob.iqds.vo.Section;

import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;
import org.pegdown.Extensions;
import org.pegdown.PegDownProcessor;

@Stateless
@Name("drugDocService")
@MeasureCalls
public class DrugDocServiceBean implements DrugDocServiceLocal {
	
	private static JaxbWrapper<DrugDocument> drugDocJaxbWrapper = JaxbWrapper.instance("hk.org.ha.model.pms.mob.iqds.vo");
	
	private static PegDownProcessor drugDocSectionProcessor = new PegDownProcessor(Extensions.NONE);
	
	private DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	
	@Logger
	private Log logger;
	
	@In
	private DrugDocManagerLocal drugDocManager;	
	
	@In
	private AccessCountLogManagerLocal accessCountLogManager;	
	
	@In
	private AllowedIpAddressesChecker allowedIpAddressesChecker;
	
	@Override
	public DrugDoc retrieveDrugDoc(HttpServletRequest req, String lang, String deviceType, String itemCode) {
		boolean chinese = (Lang.dataValueOf(lang) == Lang.TC) ;
		DrugDoc drugDoc = new DrugDoc();
		hk.org.ha.model.pms.mob.iqds.persistence.DrugDoc targetDrugDoc = drugDocManager.retrieveDrugDoc(itemCode);
		
		if ( targetDrugDoc != null ) {
			//AccessCount
			DeviceType device = DeviceType.dataValueOf(deviceType);
			accessCountLogManager.countRetrieveDrugDoc(itemCode, device, new Date());
			
			drugDoc.setDrugName(targetDrugDoc.getDrugName());

			DrugDocument drugDocument = drugDocJaxbWrapper.unmarshall(targetDrugDoc.getDrugXml());
			
			drugDoc.setDrugDocHtml(getDrugDocHtml(drugDocument, chinese));
		}
		return drugDoc;
	}

	private String getSectionListMd(List<Section> sectionList, boolean chinese){
		try {
			StringBuilder sb = new StringBuilder();
			Map<String, Section> drugDocSectionMap = new HashMap<String, Section>();
			Properties sectionHeaderProp = getSectionHeaderProp(chinese);
			
			for ( Section sec : sectionList ) {
				drugDocSectionMap.put(sec.getId(), sec);
			}
			
			for( String sectionId : retrieveDisplaySectionIdList() ){
				if( drugDocSectionMap.get(sectionId) != null ){
					Section drugDocSec = drugDocSectionMap.get(sectionId);
					
					if( chinese ){
						if( StringUtils.isNotBlank(drugDocSec.getChi()) ){
							sb.append(getFormattedSectionHeader(drugDocSec, sectionHeaderProp));
							sb.append(drugDocSec.getChi());
						}
					}else{
						if( StringUtils.isNotBlank(drugDocSec.getEng()) ){
							sb.append(getFormattedSectionHeader(drugDocSec, sectionHeaderProp));
							sb.append(drugDocSec.getEng());
						}
					}
					
				}
			}
			return sb.toString();
		}catch (Exception e) {
			logger.warn("Error when processing section mark down", e);
			return StringUtils.EMPTY;
		}
		
	}
	
	private String getDrugDocHtml(DrugDocument drugDoc, boolean chinese){
		StringBuilder sb = new StringBuilder();
		try {
			sb.append(getDrugDocHeaderMd(drugDoc, chinese));
		} catch (Exception e) {
			logger.warn("Fail to format MyMedication Header", e);
		}
		
		sb.append(getSectionListMd(drugDoc.getSectionList(), chinese));
		
		
		try {
			sb.append(getDrugDocFooterMd(drugDoc, chinese));
		} catch (Exception e) {
			logger.warn("Fail to format MyMedication Footer", e);
		}
		
		return drugDocSectionProcessor.markdownToHtml(sb.toString());
	}

	private String getDrugDocHeaderMd(DrugDocument drugDoc, boolean chinese) throws Exception{
		StringWriter writer = new StringWriter();
		InputStream in = null;
		try {
			if(chinese){
				in = Thread.currentThread().getContextClassLoader().getResourceAsStream(AppConstant.HEADER_ZHTW_PROPERTIES);
			}else{
				in = Thread.currentThread().getContextClassLoader().getResourceAsStream(AppConstant.HEADER_PROPERTIES);
			}
			IOUtils.copy(in, writer, "UTF-8");

			String headerMd = writer.toString();
			headerMd = headerMd.replace("${drugDoc.drugName}", drugDoc.getDrugName());
			return headerMd;
		}finally {
			if (in != null) {
				in.close();
			}
		}
	}

	private String getDrugDocFooterMd(DrugDocument drugDoc, boolean chinese) throws Exception{
		StringWriter writer = new StringWriter();
		InputStream in = null;
		try {
			if(chinese){
				in = Thread.currentThread().getContextClassLoader().getResourceAsStream(AppConstant.FOOTER_ZHTW_PROPERTIES);
			}else{
				in = Thread.currentThread().getContextClassLoader().getResourceAsStream(AppConstant.FOOTER_PROPERTIES);
			}
			
			IOUtils.copy(in, writer, "UTF-8");

			String footerMd = "\n\n" + writer.toString();
			footerMd = footerMd.replace("${drugDoc.version}", drugDoc.getVersion());
			footerMd = footerMd.replace("${drugDoc.revisionDate}", dateFormat.format(drugDoc.getRevisionDate()));
			
			return footerMd;
		}finally {
			if (in != null) {
				in.close();
			}
		}
	}
	
	private Properties getSectionHeaderProp(boolean chinese){
		try {
			if( chinese ){
				return PropertiesHelper.getProperties(AppConstant.SECTION_ZHTW_PROPERTIES, Thread.currentThread().getContextClassLoader(), 0);
			}else{
				return PropertiesHelper.getProperties(AppConstant.SECTION_PROPERTIES, Thread.currentThread().getContextClassLoader(), 0);
			}
		} catch (IOException e) {
			logger.warn("Failed to load file: #0, #1", e, AppConstant.SECTION_ZHTW_PROPERTIES, AppConstant.SECTION_PROPERTIES);
			return new Properties();
		}
	}
	
	private String getFormattedSectionHeader(Section section, Properties sectionHeaderProp){
		String header = sectionHeaderProp.getProperty(section.getId());
		
		if( StringUtils.isNotBlank(header) ){
			return "\n\n## "+header+"\n";
		}else{
			return "\n\n";
		}
	}
	
	private List<String> retrieveDisplaySectionIdList(){
		List<String> sectionIdList = new ArrayList<String>();
		
		String sectionString = StringUtils.EMPTY;
		
		try {
			sectionString = PropertiesHelper.getProperty(AppConstant.APPLICATION_PROPERTIES, AppConstant.DRUG_DOC_DISPLAY_SECTION_ID_LIST);
			
			return Arrays.asList(StringUtils.split(sectionString, ","));
		} catch (Exception e) {
			logger.warn("unable to read #0 under #1", 
					AppConstant.DRUG_DOC_DISPLAY_SECTION_ID_LIST, 
					AppConstant.APPLICATION_PROPERTIES);
		}
		return sectionIdList;
	}
	
	@Override
	public ReturnResult addDrugDoc(HttpServletRequest req, hk.org.ha.model.pms.mob.iqds.persistence.DrugDoc drugDoc) {
		try {
			allowedIpAddressesChecker.checkAccess(AppConstant.DRUG_DOC, req.getRemoteAddr());
		} catch (Exception e) {
			String errorMessage = "Add DrugDoc Abort. Access denied for ip address " + req.getRemoteAddr() + " for " + AppConstant.DRUG_DOC;
			logger.warn(errorMessage, e);
			return new ReturnResult(-4, errorMessage);
		}
		return drugDocManager.addDrugDoc(drugDoc);
	}

	@Override
	public ReturnResult updateDrugDoc(HttpServletRequest req, hk.org.ha.model.pms.mob.iqds.persistence.DrugDoc drugDoc) {
		try {
			allowedIpAddressesChecker.checkAccess(AppConstant.DRUG_DOC, req.getRemoteAddr());
		} catch (Exception e) {
			String errorMessage = "Update DrugDoc Abort. Access denied for ip address " + req.getRemoteAddr() + " for " + AppConstant.DRUG_DOC;
			logger.warn(errorMessage, e);
			return new ReturnResult(-4, errorMessage);
		}
		return drugDocManager.updateDrugDoc(drugDoc);
	}

	@Override
	public ReturnResult deleteDrugDoc(HttpServletRequest req, String drugName) {
		try {
			allowedIpAddressesChecker.checkAccess(AppConstant.DRUG_DOC, req.getRemoteAddr());
		} catch (Exception e) {
			String errorMessage = "Delete DrugDoc Abort. Access denied for ip address " + req.getRemoteAddr() + " for " + AppConstant.DRUG_DOC;
			logger.warn(errorMessage, e);
			return new ReturnResult(-4, errorMessage);
		}
		return drugDocManager.deleteDrugDoc(drugName);
	}
	
}
