package hk.org.ha.service.biz.pms.iqds.interfaces;

import hk.org.ha.model.pms.mob.iqds.persistence.Workstore;

import java.util.Date;

import org.osoa.sca.annotations.OneWay;

public interface IqdsTicketProcessorJmsRemote {

	@OneWay
	void processTicket(Workstore workstore, Date ticketDate, Integer ticketNum);	

}
