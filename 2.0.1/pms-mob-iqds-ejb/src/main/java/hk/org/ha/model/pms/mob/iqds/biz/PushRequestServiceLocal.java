package hk.org.ha.model.pms.mob.iqds.biz;
import hk.org.ha.model.pms.mob.iqds.vo.PushRequest;
import hk.org.ha.model.pms.mob.iqds.vo.ReturnResult;

import java.util.Collection;

import javax.ejb.Local;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.Consumes;

@Local
@Path("/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface PushRequestServiceLocal {
	
	
	@GET
	@Path("pushRequests")
	Collection<PushRequest> retrievePushRequests(
			@HeaderParam("deviceUuid") @DefaultValue("-") String deviceUuid,
			@HeaderParam("deviceToken") String deviceToken,
			@HeaderParam("deviceType") @DefaultValue("I") String deviceType,
			@HeaderParam("lang") @DefaultValue("EN") String lang);
	
	@POST
	@Path("pushRequests")
	ReturnResult addPushRequest(
			@HeaderParam("deviceUuid") @DefaultValue("-") String deviceUuid,
			@HeaderParam("deviceToken") String deviceToken,
			@HeaderParam("deviceType") @DefaultValue("I") String deviceType,
			@HeaderParam("lang") @DefaultValue("EN") String lang,		
			PushRequest pushRequest);
	
	@DELETE
	@Path("pushRequest/{pushRequestId}")
	ReturnResult deletePushRequest(
			@HeaderParam("deviceUuid") @DefaultValue("-") String deviceUuid,
			@HeaderParam("deviceToken") String deviceToken,
			@HeaderParam("deviceType") @DefaultValue("I") String deviceType,
			@HeaderParam("lang") @DefaultValue("EN") String lang,		
			@PathParam("pushRequestId") Long pushRequestId);
}
