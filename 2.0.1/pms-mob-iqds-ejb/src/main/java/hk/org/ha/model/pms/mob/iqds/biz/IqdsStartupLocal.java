package hk.org.ha.model.pms.mob.iqds.biz;

import javax.ejb.Local;
import org.jboss.seam.annotations.async.Asynchronous;
import org.jboss.seam.annotations.async.Duration;

@Local
public interface IqdsStartupLocal {
	
	@Asynchronous
	void startup(@Duration Long delayDuration);
	
}
