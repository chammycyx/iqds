package hk.org.ha.model.pms.mob.iqds.biz.disp;
import hk.org.ha.model.pms.mob.iqds.persistence.Ticket;
import hk.org.ha.model.pms.mob.iqds.persistence.TicketStatus;
import hk.org.ha.model.pms.mob.iqds.persistence.Workstore;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.ejb.Local;

@Local
public interface TicketManagerLocal {

	Collection<Ticket> retrieveTickets(			
			Workstore workstore,
			Date ticketDate);
		
	List<Ticket> retrievePendingTickets(
			Workstore workstore,
			Date ticketDate,
			List<Integer> issueWinNumList);

	Ticket retrieveAndLockTicket(
			Workstore workstore,
			Date ticketDate,
			Integer ticketNum);

	Ticket retrieveAndLockTicket(
			Workstore workstore, 
			Date ticketDate, 
			Integer ticketNum, 
			List<TicketStatus> ticketStatusList);
	
	void issueTicket(
			Workstore workstore,
			Date ticketDate,
			Integer ticketNum, 
			Integer issueWindowNum);
	
	boolean collectTicket(
			Workstore workstore,
			Date ticketDate,
			Integer ticketNum);

	void invalidDispBoard(String hospCode, String workstoreCode);
}
