package hk.org.ha.model.pms.mob.iqds.biz;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import hk.org.ha.fmk.pms.util.PropertiesHelper;
import hk.org.ha.fmk.pms.web.MeasureCalls;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@AutoCreate
@Name("allowedIpAddressesChecker")
@Scope(ScopeType.APPLICATION)
@MeasureCalls
public class AllowedIpAddressesChecker {
	
	private Map<String, List<Pattern>> patternMap = new HashMap<String, List<Pattern>>();

	public void checkAccess(String prefix, String remoteAddr) {
		if (!isAllowedIpAddress(prefix, remoteAddr)) {
			throw new RuntimeException("access denied for ip address " + remoteAddr + " for " + prefix);
		}
	}

	private boolean isAllowedIpAddress(String prefix, String remoteAddr) {
		
		try {
			List<Pattern> patternList = null;
			
			String allowedIpAddresses = PropertiesHelper.getProperty(
					AppConstant.APPLICATION_PROPERTIES, 
					prefix + "." + AppConstant.ALLOWED_IP_ADDRESSES);

			synchronized (this) {				
				patternList = patternMap.get(allowedIpAddresses);			
				if (patternList == null) {
					patternList = new ArrayList<Pattern>();
					for (String ipaddress : allowedIpAddresses.split("[,;\\s]+")) {
						patternList.add(Pattern.compile(wildcardToRegex(ipaddress)));					
					}
					patternMap.put(allowedIpAddresses, patternList);
				}
			}
			
			for (Pattern pattern : patternList) {
				if (pattern.matcher(remoteAddr).matches()) {
					return true;
				}
			}
			return false;
			
		} catch (IOException e) {
			return false;
		}
	}

	/*
	 * translate from wildcard to regular expression pattern 
	 * eg. 192.168.12 > "^192\\.168\\..*$" 
	 */
    private String wildcardToRegex(String wildcard){
        StringBuffer s = new StringBuffer(wildcard.length());
        s.append('^');
        for (int i = 0, is = wildcard.length(); i < is; i++) {
            char c = wildcard.charAt(i);
            switch(c) {
                case '*':
                    s.append(".*");
                    break;
                case '?':
                    s.append(".");
                    break;
                    // escape special regexp-characters
                case '(': case ')': case '[': case ']': case '$':
                case '^': case '.': case '{': case '}': case '|':
                case '\\':
                    s.append("\\");
                    s.append(c);
                    break;
                default:
                    s.append(c);
                    break;
            }
        }
        s.append('$');
        return(s.toString());
    }	
}
