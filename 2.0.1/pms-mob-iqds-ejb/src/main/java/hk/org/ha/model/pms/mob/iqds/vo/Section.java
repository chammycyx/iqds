package hk.org.ha.model.pms.mob.iqds.vo;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder={"eng", "chi"})
public class Section implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@XmlElement
	private String eng;
	
	@XmlElement
	private String chi;

	@XmlAttribute
	private String id;

	public String getEng() {
		return eng;
	}

	public void setEng(String eng) {
		this.eng = eng;
	}

	public String getChi() {
		return chi;
	}

	public void setChi(String chi) {
		this.chi = chi;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}