package hk.org.ha.model.pms.mob.iqds.biz;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.mob.iqds.biz.disp.TicketManagerLocal;
import hk.org.ha.model.pms.mob.iqds.biz.push.ApnsSender;
import hk.org.ha.model.pms.mob.iqds.biz.push.GcmSender;
import hk.org.ha.model.pms.mob.iqds.biz.push.PushRequestManagerLocal;
import hk.org.ha.model.pms.mob.iqds.persistence.DeviceType;
import hk.org.ha.model.pms.mob.iqds.persistence.Lang;
import hk.org.ha.model.pms.mob.iqds.persistence.PushRequest;
import hk.org.ha.model.pms.mob.iqds.persistence.RecordStatus;
import hk.org.ha.model.pms.mob.iqds.persistence.Ticket;
import hk.org.ha.model.pms.mob.iqds.persistence.Workstore;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.core.Interpolator;
import org.jboss.seam.log.Log;

@Stateless
@Name("iqdsTicketProcessor")
@MeasureCalls
public class IqdsTicketProcessorBean implements IqdsTicketProcessorLocal {

	private static final String RESOURCES = "resources";
	private static final String DOT_PROPERTIES = ".properties";
	
	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;	
	
	@In
	private TicketManagerLocal ticketManager;
	
	@In
	private PushRequestManagerLocal pushRequestManager;

	@In
	private ApnsSender apnsSender;
	
	@In
	private GcmSender gcmSender;

	@In
	private Interpolator interpolator;
	
	private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	private Map<Lang, Properties> resourceMap = new HashMap<Lang, Properties>();
	 
	public IqdsTicketProcessorBean() throws IOException {
		resourceMap.put(Lang.EN, readProperties(RESOURCES + DOT_PROPERTIES));
		resourceMap.put(Lang.TC, readProperties(RESOURCES + "_zh_TW" + DOT_PROPERTIES));
	}
		
	public void processTicket(Workstore workstore, Date ticketDate, Integer ticketNum) {
		
		logger.info("process ticket [#0,#1,#2,#3] for push notification.", 
				workstore.getHospCode(), 
				workstore.getWorkstoreCode(),
				dateFormat.format(ticketDate),
				ticketNum);
				
		Ticket ticket = ticketManager.retrieveAndLockTicket(workstore, ticketDate, ticketNum);

		if (ticket == null) {
			throw new RuntimeException("UNEXPECTED ERROR : ticket not found!");
		}
		
		List<PushRequest> pushRequestList =  pushRequestManager.retrievePushRequests(workstore, ticketDate, ticketNum);
		
		if (!pushRequestList.isEmpty()) {
			
			Map<Long,Integer> prevUncollectCountMap = buildUncollectCountMap(pushRequestList, ticketDate);
						
			// group up the devices with same language and badge for notification
			Date today = new Date();							
			Map<Key,List<String>> deviceTokenMap = new HashMap<Key, List<String>>();
			for (PushRequest pushRequest : pushRequestList) {			
				
				if (pushRequest.getDevice().getToken() != null) {
					Integer prevUncollectCount = prevUncollectCountMap.get(pushRequest.getDevice().getId());
	
					Key key = new Key(
							pushRequest.getDevice().getType(),
							pushRequest.getDevice().getLang(),
							Integer.valueOf( 1 + (prevUncollectCount != null ? prevUncollectCount.intValue() : 0 )));				
					
					List<String> deviceTokens = deviceTokenMap.get(key);
					if (deviceTokens == null) {
						deviceTokens = new ArrayList<String>();
						deviceTokenMap.put(key,deviceTokens);
					}
					String deviceToken = pushRequest.getDevice().getToken();
					if (!deviceTokens.contains(deviceToken)) {
						deviceTokens.add(deviceToken);
					}
				}
				
				pushRequest.setStatus(RecordStatus.Complete);
				pushRequest.setCompleteDate(today);
				pushRequest.setTicket(ticket);
			}
			
			em.flush();
			
			if( RecordStatus.Active == workstore.getStatus() ){			
				try {
					
					for (Map.Entry<Key, List<String>> entry : deviceTokenMap.entrySet()) {
						
						String workstoreName = (Lang.TC == entry.getKey().getLang()) ? 
								workstore.getNameTc() : workstore.getName();
								
						String messageKey = "collect_msg";
						if( AppConstant.SINGLE_QUEUE_WINDOW_NUM.equals(ticket.getIssueWindowNum()) ){
							messageKey = "singleq_collect_msg";
						}else if( AppConstant.ENQUIRY_WINDOW_NUM_LIST.contains(ticket.getIssueWindowNum()) ){
							messageKey = "enquiry_msg";
						}
						
						String resourceMssage = resourceMap.get(entry.getKey().getLang()).getProperty(messageKey);
						String message = interpolator.interpolate(resourceMssage, 
									workstoreName, ticket.getTicketNum(), ticket.getIssueWindowNum());
	
						logger.info("send message [#0,#1,#2], badge [#3] to #4 #5 device(s).", 
								message, 
								ticket.getTicketNum(),
								ticket.getIssueWindowNum(),
								entry.getKey().getBadge().intValue(), 
								entry.getValue().size(),
								entry.getKey().getType().getDisplayValue());
						
						switch (entry.getKey().getType()) 
						{
							case iOS :
								apnsSender.sendNotifications(
										message, 
										entry.getKey().getBadge().intValue(),
										entry.getValue());
								break;
							case Android :
								gcmSender.sendNotifications(
										message, 
										entry.getKey().getBadge().intValue(),
										entry.getValue());
								break;
						}
					}
									
				} catch (Exception e) {
					logger.error("send message error!", e);
				}
			}
		}
	}
			
	private Map<Long,Integer> buildUncollectCountMap(List<PushRequest> pushRequestList, Date ticketDate) {
		List<Long> deviceIds = new ArrayList<Long>();
		for (PushRequest pushRequest : pushRequestList) {
			deviceIds.add(pushRequest.getDevice().getId());
		}
		return pushRequestManager.retrieveUncollectCount(deviceIds, ticketDate);
	}
	
	private Properties readProperties(String resourceFileName) throws IOException {		
		Properties properties = null;
		InputStream in = null;
		try {
			in = Thread.currentThread().getContextClassLoader().getResourceAsStream(resourceFileName);		
			properties = new Properties();
			properties.load(new InputStreamReader(in, "UTF-8"));
		} finally {
			if (in != null) {
				in.close();
			}
		}
		return properties;		
	}	
	
	public static class Key {
		
		private DeviceType type;
		private Lang lang;
		private Integer badge;
		
		public Key(DeviceType type, Lang lang, Integer badge) {
			super();
			this.type = type;
			this.lang = lang;
			this.badge = badge;
		}
		
		public DeviceType getType() {
			return type;
		}
		public void setType(DeviceType type) {
			this.type = type;
		}
		public Lang getLang() {
			return lang;
		}
		public void setLang(Lang lang) {
			this.lang = lang;
		}
		public Integer getBadge() {
			return badge;
		}
		public void setBadge(Integer badge) {
			this.badge = badge;
		}
		
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((badge == null) ? 0 : badge.hashCode());
			result = prime * result + ((lang == null) ? 0 : lang.hashCode());
			result = prime * result + ((type == null) ? 0 : type.hashCode());
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Key other = (Key) obj;
			if (badge == null) {
				if (other.badge != null)
					return false;
			} else if (!badge.equals(other.badge))
				return false;
			if (lang == null) {
				if (other.lang != null)
					return false;
			} else if (!lang.equals(other.lang))
				return false;
			if (type == null) {
				if (other.type != null)
					return false;
			} else if (!type.equals(other.type))
				return false;
			return true;
		}
	}
}
