@echo off

if "%DATABASE_ID%" == "" (
	set DATABASE_ID=%WORKSTATION_ID%
) 

for %%* in (.) do set CURR_FOLDER=%%~n*

@echo on

cd %CURR_FOLDER%-ejb

call mvn clean test -Pembedded %*

cd ..