create or replace procedure change_constraint_state (m varchar2) as 
  cursor constraints is 
	select c.TABLE_NAME, c.CONSTRAINT_NAME from USER_CONSTRAINTS c, USER_TABLES u 
  where c.TABLE_NAME=u.TABLE_NAME and c.CONSTRAINT_TYPE != 'P'; 
begin 
  if (lower(m) != 'enable' and lower(m) != 'disable') 
  then 
    dbms_output.put_line('mode must be enable or disable'); 
    return; 
  end if; 
  for rec in constraints loop 
    execute immediate 'alter table ' || rec.TABLE_NAME || ' ' || m || ' constraint ' || rec.CONSTRAINT_NAME; 
  end loop; 
end change_constraint_state;