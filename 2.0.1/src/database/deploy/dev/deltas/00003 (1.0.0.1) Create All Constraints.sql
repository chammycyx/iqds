alter table PUSH_REQUEST add constraint FK_PUSH_REQUEST_01 foreign key (DEVICE_ID) references DEVICE (ID);
alter table PUSH_REQUEST add constraint FK_PUSH_REQUEST_02 foreign key (TICKET_ID) references TICKET (ID);
alter table PUSH_REQUEST add constraint FK_PUSH_REQUEST_03 foreign key (HOSP_CODE, WORKSTORE_CODE) references WORKSTORE (HOSP_CODE, WORKSTORE_CODE);

alter table TICKET add constraint FK_TICKET_01 foreign key (HOSP_CODE, WORKSTORE_CODE) references WORKSTORE (HOSP_CODE, WORKSTORE_CODE);


--//@UNDO
alter table PUSH_REQUEST drop constraint FK_PUSH_REQUEST_01;
alter table PUSH_REQUEST drop constraint FK_PUSH_REQUEST_02;
alter table PUSH_REQUEST drop constraint FK_PUSH_REQUEST_03;

alter table TICKET drop constraint FK_TICKET_01;
--//