create index FK_PUSH_REQUEST_01 on PUSH_REQUEST (DEVICE_ID) tablespace IQDS_DATA_01;
create index FK_PUSH_REQUEST_02 on PUSH_REQUEST (TICKET_ID) tablespace IQDS_DATA_01;
create index FK_PUSH_REQUEST_03 on PUSH_REQUEST (HOSP_CODE, WORKSTORE_CODE) tablespace IQDS_DATA_01;

create index FK_TICKET_01 on TICKET (HOSP_CODE, WORKSTORE_CODE) tablespace IQDS_DATA_01;


--//@UNDO
drop index FK_PUSH_REQUEST_01;
drop index FK_PUSH_REQUEST_02;
drop index FK_PUSH_REQUEST_03;

drop index FK_TICKET_01;
--//