create sequence SQ_AUDIT_LOG start with 100000000;
create sequence SQ_DEVICE increment by 50 start with 100000049;
create sequence SQ_INFO_LINK increment by 50 start with 100000049;
create sequence SQ_PUSH_REQUEST increment by 50 start with 100000049;
create sequence SQ_TICKET increment by 50 start with 100000049;


--//@UNDO
drop sequence SQ_AUDIT_LOG;
drop sequence SQ_DEVICE;
drop sequence SQ_INFO_LINK;
drop sequence SQ_PUSH_REQUEST;
drop sequence SQ_TICKET;
--//