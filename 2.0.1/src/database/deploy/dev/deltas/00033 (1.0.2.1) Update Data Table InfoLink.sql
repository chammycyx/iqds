alter table INFO_LINK modify URL varchar2(150);
alter table INFO_LINK add HYPERLINK varchar2(150) null;
alter table INFO_LINK add HYPERLINK_TC varchar2(150) null;

delete from INFO_LINK;

insert into INFO_LINK (ID,NAME_TC,NAME,SORT_SEQ,URL,HYPERLINK,HYPERLINK_TC) values (1,'藥物資訊','Drug Information',1,'http://www21.ha.org.hk/smartpatient/tc/healthinfo_gallery/health_printmatters/detail.html?code=hig_pri_pam_dru&id=9','http://www21.ha.org.hk/smartpatient/en/healthinfo_gallery/health_printmatters/detail.html?code=hig_pri_pam_dru&id=9','http://www21.ha.org.hk/smartpatient/tc/healthinfo_gallery/health_printmatters/detail.html?code=hig_pri_pam_dru&id=9');
insert into INFO_LINK (ID,NAME_TC,NAME,SORT_SEQ,URL,HYPERLINK,HYPERLINK_TC) values (2,'疾病相關資訊','Disease-related Information',2,'http://www21.ha.org.hk/smartpatient/tc/morediseases_conditions.html', 'http://www21.ha.org.hk/smartpatient/en/morediseases_conditions.html','http://www21.ha.org.hk/smartpatient/tc/morediseases_conditions.html');
insert into INFO_LINK (ID,NAME_TC,NAME,SORT_SEQ,URL,HYPERLINK,HYPERLINK_TC) values (3,'醫院管理局','Hospital Authority',3,'http://www.ha.org.hk/visitor/ha_index.asp?Lang=CHIB5', 'http://www.ha.org.hk/visitor/ha_index.asp?Content_ID=0&Lang=ENG&Dimension=100&Ver=HTML','http://www.ha.org.hk/visitor/ha_index.asp?Lang=CHIB5');
insert into INFO_LINK (ID,NAME_TC,NAME,SORT_SEQ,URL,HYPERLINK,HYPERLINK_TC) values (4,'智友站','Smart Patient',4,'http://www21.ha.org.hk/smartpatient/tc/home.html', 'http://www21.ha.org.hk/smartpatient/en/home.html','http://www21.ha.org.hk/smartpatient/tc/home.html');
insert into INFO_LINK (ID,NAME_TC,NAME,SORT_SEQ,URL,HYPERLINK,HYPERLINK_TC) values (5,'衞生署','Department of Health',5,'http://www.dh.gov.hk/cindex.html', 'http://www.dh.gov.hk/eindex.html','http://www.dh.gov.hk/cindex.html');
insert into INFO_LINK (ID,NAME_TC,NAME,SORT_SEQ,URL,HYPERLINK,HYPERLINK_TC) values (6,'衞生署藥物辦公室','Department of Health Drug Office',6,'http://www.drugoffice.gov.hk/eps/do/tc/level.html', 'http://www.drugoffice.gov.hk/eps/do/en/level.html','http://www.drugoffice.gov.hk/eps/do/tc/level.html');

alter table INFO_LINK modify HYPERLINK not null;
alter table INFO_LINK modify HYPERLINK_TC not null;

--//@UNDO
alter table INFO_LINK DROP column HYPERLINK;
alter table INFO_LINK DROP column HYPERLINK_TC;
--//
