create table DEVICE (
    ID number(19) not null,
    UUID varchar2(36) not null,
    TOKEN varchar2(1000) null,
    TYPE varchar2(1) not null,
    LANG varchar2(2) not null,
    CREATE_DATE timestamp not null,
    constraint PK_DEVICE primary key (ID) using index tablespace IQDS_INDX_01)
tablespace IQDS_DATA_01;

create table INFO_LINK (
    ID number(19) not null,
    NAME varchar2(100) not null,
    NAME_TC varchar2(300) not null,
    URL varchar2(100) not null,
    SORT_SEQ number(10) not null,
    constraint PK_INFO_LINK primary key (ID) using index tablespace IQDS_INDX_01)
tablespace IQDS_DATA_01;

create table PUSH_REQUEST (
    ID number(19) not null,
    DEVICE_ID number(19) not null,
    HOSP_CODE varchar2(3) not null,
    WORKSTORE_CODE varchar2(4) not null,
    TICKET_DATE date not null,
    TICKET_NUM number(10) not null,
    STATUS varchar2(1) not null,
    TICKET_ID number(19) null,
    CREATE_DATE timestamp not null,
    COMPLETE_DATE timestamp null,
    constraint PK_PUSH_REQUEST primary key (ID) using index tablespace IQDS_INDX_01)
tablespace IQDS_DATA_01;

create table TICKET (
    ID number(19) not null,
    HOSP_CODE varchar2(3) not null,
    WORKSTORE_CODE varchar2(4) not null,
    TICKET_DATE date not null,
    TICKET_NUM number(10) not null,
    ISSUE_WINDOW_NUM number(10) not null,
    ISSUE_DATE timestamp not null,
    COLLECT_DATE timestamp null,
    constraint PK_TICKET primary key (ID) using index tablespace IQDS_INDX_01)
tablespace IQDS_DATA_01;

create table WORKSTORE (
    HOSP_CODE varchar2(3) not null,
    WORKSTORE_CODE varchar2(4) not null,
    NAME varchar2(100) not null,
    NAME_TC varchar2(300) not null,
    ADDRESS varchar2(500) not null,
    ADDRESS_TC varchar2(1500) not null,
    CONTACT varchar2(100) null,
    SERVICE_HOUR varchar2(500) null,
    SERVICE_HOUR_TC varchar2(1500) null,
    REMARK varchar2(1000) null,
    REMARK_TC varchar2(2000) null,
    IMAGE_URL varchar2(100) null,
    LATITUDE number(9,6) not null,
    LONGITUDE number(9,6) not null,
    AREA varchar2(255) not null,
    SORT_SEQ number(10) not null,
    TIMEOUT_INTERVAL number(10) not null,
    SHOW_TIMEOUT_FLAG number(1) default 0 not null,
    STATUS varchar2(1) not null,
    CREATE_DATE timestamp not null,
    constraint PK_WORKSTORE primary key (HOSP_CODE, WORKSTORE_CODE) using index tablespace IQDS_INDX_01)
tablespace IQDS_DATA_01;


--//@UNDO
drop table DEVICE;
drop table INFO_LINK;
drop table PUSH_REQUEST;
drop table TICKET;
drop table WORKSTORE;
--//