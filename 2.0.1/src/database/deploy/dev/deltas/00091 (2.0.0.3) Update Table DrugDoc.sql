alter table DRUG_DOC add DRUG_XML clob null;

alter table DRUG_DOC drop column DRUG_DOC_MD;
alter table DRUG_DOC drop column DRUG_DOC_MD_CHI;

--//@UNDO
alter table DRUG_DOC drop column DRUG_XML;

alter table DRUG_DOC add DRUG_DOC_MD clob null;
alter table DRUG_DOC add DRUG_DOC_MD_CHI clob null;
--//