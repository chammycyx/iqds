create table DRUG_DOC_MAPPING (
    ITEM_CODE varchar2(6) not null,
    DRUG_NAME varchar2(100) not null,
    constraint PK_DRUG_DOC_MAPPING primary key (ITEM_CODE) using index tablespace IQDS_LOG_INDX_01)
tablespace IQDS_LOG_INDX_01;

comment on table  DRUG_DOC_MAPPING is 'Drug document mapping';
comment on column DRUG_DOC_MAPPING.ITEM_CODE is 'Item code';
comment on column DRUG_DOC_MAPPING.DRUG_NAME is 'Drug name';

--//@UNDO
drop table DRUG_DOC_MAPPING;
--//