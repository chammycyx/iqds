update WORKSTORE
set SERVICE_HOUR = 'Monday to Friday:'||chr(10)||'8:45 AM to 1:00 PM'||chr(10)||'2:00 PM to 7:30 PM'||chr(10)||''||chr(10)||'Saturday:'||chr(10)||'9:00 AM to 1:00 PM'||chr(10)||''||chr(10)||'Sunday and Public Holidays:'||chr(10)||'Closed',
SERVICE_HOUR_TC = '星期一至五:'||chr(10)||'上午 8:45 至下午 1:00'||chr(10)||'下午 2:00 至下午 7:30'||chr(10)||''||chr(10)||'星期六:'||chr(10)||'上午 9:00 至下午1:00'||chr(10)||''||chr(10)||'星期日及公眾假期:'||chr(10)||'休息'
where HOSP_CODE = 'KH' and WORKSTORE_CODE = 'RWKS';

update WORKSTORE
set SERVICE_HOUR = 'Monday to Friday:'||chr(10)||'8:45 AM to 1:00 PM'||chr(10)||'2:00 PM to 7:30 PM'||chr(10)||''||chr(10)||'Saturday:'||chr(10)||'9:00 AM to 1:00 PM'||chr(10)||''||chr(10)||'Sunday and Public Holidays:'||chr(10)||'Closed',
SERVICE_HOUR_TC = '星期一至五:'||chr(10)||'上午 8:45 至下午 1:00'||chr(10)||'下午 2:00 至下午 7:30'||chr(10)||''||chr(10)||'星期六:'||chr(10)||'上午 9:00 至下午1:00'||chr(10)||''||chr(10)||'星期日及公眾假期:'||chr(10)||'休息'
where HOSP_CODE = 'KH' and WORKSTORE_CODE = 'WKSO';

--//@UNDO
--//
