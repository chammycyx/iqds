create index I_TICKET_01 on TICKET (HOSP_CODE, WORKSTORE_CODE, TICKET_DATE, TICKET_NUM) tablespace IQDS_DATA_01;

create index I_DEVICE_01 on DEVICE (UUID) tablespace IQDS_DATA_01;

create index I_PUSH_REQUEST_01 on PUSH_REQUEST (DEVICE_ID, TICKET_DATE) tablespace IQDS_DATA_01;
create index I_PUSH_REQUEST_02 on PUSH_REQUEST (HOSP_CODE, WORKSTORE_CODE, TICKET_DATE, TICKET_NUM) tablespace IQDS_DATA_01;


--//@UNDO
drop index I_TICKET_01;

drop index I_DEVICE_01;

drop index I_PUSH_REQUEST_01;
drop index I_PUSH_REQUEST_02;
--//