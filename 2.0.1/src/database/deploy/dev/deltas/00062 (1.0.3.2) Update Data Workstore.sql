update WORKSTORE
set REMARK = 'Please collect medications on the same day when the prescription is received. The prescription is valid for 4 working days.'
where HOSP_CODE = 'RH';

update WORKSTORE
set ADDRESS = 'G/F, F block, Queen Elizabeth Hospital, 30 Gascoigne Road, Kowloon'
where HOSP_CODE = 'QEH' and WORKSTORE_CODE = 'WKSF';

insert into WORKSTORE (HOSP_CODE,WORKSTORE_CODE,NAME,NAME_TC,ADDRESS,ADDRESS_TC,CONTACT,SERVICE_HOUR,SERVICE_HOUR_TC,REMARK,REMARK_TC,IMAGE_URL,LATITUDE,LONGITUDE,AREA,SORT_SEQ,TIMEOUT_INTERVAL,SHOW_TIMEOUT_FLAG,STATUS,CREATE_DATE, WINDOW_TICKET_COUNT) values
('TWE','WKS',
'Tung Wah Eastern Hospital Pharmacy Department',
'東華東院藥劑部',
'G/F, Main Block, 19 Eastern Hospital Road, Causeway Bay, Hong Kong',
'香港銅鑼灣東院道19號，主座大樓地下',NULL,
'Monday to Friday:'||chr(10)||'8:30 AM to 7:00 PM'||chr(10)||''||chr(10)||'Saturday:'||chr(10)||'9:00 AM to 1:00 PM'||chr(10)||''||chr(10)||'Sunday and Public Holidays:'||chr(10)||'Closed',
'星期一至五:'||chr(10)||'上午 8:30 至下午 7:00'||chr(10)||''||chr(10)||'星期六:'||chr(10)||'上午 9:00 至下午 1:00'||chr(10)||''||chr(10)||'星期日及公眾假期:'||chr(10)||'休息',
'Please collect medications on the same day when the prescription is received. The prescription is valid for 4 working days.',
'請於領取藥單當天取藥，藥單有效期為四個工作天。',
'/images/TWE_WKS_01.png',
22.274749, 114.190289,
'HK',
0,
1,
1,
'A',to_timestamp('31-JUL-15 12.00.00.000000000 AM','DD-MON-RR HH.MI.SS.FF AM'),
18);

insert into WORKSTORE (HOSP_CODE,WORKSTORE_CODE,NAME,NAME_TC,ADDRESS,ADDRESS_TC,CONTACT,SERVICE_HOUR,SERVICE_HOUR_TC,REMARK,REMARK_TC,IMAGE_URL,LATITUDE,LONGITUDE,AREA,SORT_SEQ,TIMEOUT_INTERVAL,SHOW_TIMEOUT_FLAG,STATUS,CREATE_DATE, WINDOW_TICKET_COUNT) values
('QEH','WKSE',
'Queen Elizabeth Hospital E Block 1/F Out-Patient Pharmacy',
'伊利沙伯醫院E座一樓藥房',
'1/F, Block E, Queen Elizabeth Hospital, 30 Gascoigne Road, Kowloon',
'九龍加士居道30號伊利沙伯醫院E座一樓',NULL,
'Monday to Friday:'||chr(10)||'9:00 AM to 1:00 PM'||chr(10)||'2:00 PM to 5:00 PM'||chr(10)||''||chr(10)||'Saturday, Sunday and Public Holidays:'||chr(10)||'Closed',
'星期一至五:'||chr(10)||'上午 9:00 至下午 1:00'||chr(10)||'上午 2:00 至下午 5:00'||chr(10)||''||chr(10)||'星期六、日及公眾假期:'||chr(10)||'休息',
'Please collect medications on the same day when prescription is received. The prescription is valid for 4 working days.'||chr(10)||''||chr(10)||'Please queue up for medication collection after the number has been called.',
'請於領取藥單當天取藥，藥單有效期為四個工作天。'||chr(10)||''||chr(10)||'已叫籌號者，請到取藥處排隊取藥。',
'/images/QEH_WKSE_01.png',
22.309346, 114.175164,
'KLN',
0,
2,
1,
'A',to_timestamp('31-JUL-15 12.00.00.000000000 AM','DD-MON-RR HH.MI.SS.FF AM'),
8);

insert into WORKSTORE (HOSP_CODE,WORKSTORE_CODE,NAME,NAME_TC,ADDRESS,ADDRESS_TC,CONTACT,SERVICE_HOUR,SERVICE_HOUR_TC,REMARK,REMARK_TC,IMAGE_URL,LATITUDE,LONGITUDE,AREA,SORT_SEQ,TIMEOUT_INTERVAL,SHOW_TIMEOUT_FLAG,STATUS,CREATE_DATE, WINDOW_TICKET_COUNT) values
('YFS','WKS',
'Kwun Tong Yung Fung Shee Memorial Centre Specialist Out-Patient Clinic Pharmacy',
'觀塘容鳳書紀念中心專科門診藥劑部',
'G/F, 79 Cha Kwo Ling Road, Kwun Tong, Kowloon',
'九龍觀塘茶果嶺道79號地下',NULL,
'Monday to Friday:'||chr(10)||'8:52 AM to 1:00 PM'||chr(10)||'2:00 PM to 5:40 PM'||chr(10)||''||chr(10)||'Saturday, Sunday and Public Holidays:'||chr(10)||'Closed',
'星期一至五:'||chr(10)||'上午 8:52 至下午 1:00'||chr(10)||'上午 2:00 至下午 5:40'||chr(10)||''||chr(10)||'星期六、日及公眾假期:'||chr(10)||'休息',
'Please collect medications on the same day when the prescription is received. The prescription is valid for 4 working days.',
'請於領取藥單當天取藥，藥單有效期為四個工作天。',
'/images/YFS_WKS_01.png',
22.308053, 114.228553,
'KLN',
0,
2,
1,
'A',to_timestamp('31-JUL-15 12.00.00.000000000 AM','DD-MON-RR HH.MI.SS.FF AM'),
8);

update WORKSTORE set SORT_SEQ=1 where HOSP_CODE='WPC' and WORKSTORE_CODE='WKS';
update WORKSTORE set SORT_SEQ=2 where HOSP_CODE='GH' and WORKSTORE_CODE='WKS';
update WORKSTORE set SORT_SEQ=3 where HOSP_CODE='PYN' and WORKSTORE_CODE='WKS1';
update WORKSTORE set SORT_SEQ=4 where HOSP_CODE='PYN' and WORKSTORE_CODE='WKS2';
update WORKSTORE set SORT_SEQ=5 where HOSP_CODE='QMH' and WORKSTORE_CODE='WKS1';
update WORKSTORE set SORT_SEQ=6 where HOSP_CODE='RH' and WORKSTORE_CODE='WKS';
update WORKSTORE set SORT_SEQ=7 where HOSP_CODE='TWE' and WORKSTORE_CODE='WKS';
update WORKSTORE set SORT_SEQ=8 where HOSP_CODE='TWH' and WORKSTORE_CODE='WKS1';
update WORKSTORE set SORT_SEQ=9 where HOSP_CODE='CMC' and WORKSTORE_CODE='WKS2';
update WORKSTORE set SORT_SEQ=10 where HOSP_CODE='HKE' and WORKSTORE_CODE='WKS';
update WORKSTORE set SORT_SEQ=11 where HOSP_CODE='KH' and WORKSTORE_CODE='RWKS';
update WORKSTORE set SORT_SEQ=12 where HOSP_CODE='KH' and WORKSTORE_CODE='WKSO';
update WORKSTORE set SORT_SEQ=13 where HOSP_CODE='KWH' and WORKSTORE_CODE='WKS';
update WORKSTORE set SORT_SEQ=14 where HOSP_CODE='OLM' and WORKSTORE_CODE='WKS';
update WORKSTORE set SORT_SEQ=15 where HOSP_CODE='PMH' and WORKSTORE_CODE='WKS';
update WORKSTORE set SORT_SEQ=16 where HOSP_CODE='PMH' and WORKSTORE_CODE='SWKS';
update WORKSTORE set SORT_SEQ=17 where HOSP_CODE='QES' and WORKSTORE_CODE='WKS';
update WORKSTORE set SORT_SEQ=18 where HOSP_CODE='QEH' and WORKSTORE_CODE='WKSE';
update WORKSTORE set SORT_SEQ=19 where HOSP_CODE='QEH' and WORKSTORE_CODE='WKSF';
update WORKSTORE set SORT_SEQ=20 where HOSP_CODE='UCH' and WORKSTORE_CODE='WKS1';
update WORKSTORE set SORT_SEQ=21 where HOSP_CODE='YFS' and WORKSTORE_CODE='WKS';
update WORKSTORE set SORT_SEQ=22 where HOSP_CODE='YCH' and WORKSTORE_CODE='WKS2';
update WORKSTORE set SORT_SEQ=23 where HOSP_CODE='AHN' and WORKSTORE_CODE='WKS2';
update WORKSTORE set SORT_SEQ=24 where HOSP_CODE='CPH' and WORKSTORE_CODE='WKSO';
update WORKSTORE set SORT_SEQ=25 where HOSP_CODE='PWH' and WORKSTORE_CODE='WKL';
update WORKSTORE set SORT_SEQ=26 where HOSP_CODE='NDH' and WORKSTORE_CODE='WKS2';
update WORKSTORE set SORT_SEQ=27 where HOSP_CODE='NLT' and WORKSTORE_CODE='WKS';
update WORKSTORE set SORT_SEQ=28 where HOSP_CODE='POH' and WORKSTORE_CODE='WKS1';
update WORKSTORE set SORT_SEQ=29 where HOSP_CODE='PWH' and WORKSTORE_CODE='WKS';
update WORKSTORE set SORT_SEQ=30 where HOSP_CODE='TKO' and WORKSTORE_CODE='WKS2';
update WORKSTORE set SORT_SEQ=31 where HOSP_CODE='TKO' and WORKSTORE_CODE='WKS1';
update WORKSTORE set SORT_SEQ=32 where HOSP_CODE='TMH' and WORKSTORE_CODE='YOP';
update WORKSTORE set SORT_SEQ=33 where HOSP_CODE='TMH' and WORKSTORE_CODE='ACC';
update WORKSTORE set SORT_SEQ=34 where HOSP_CODE='TMH' and WORKSTORE_CODE='WKS';

--//@UNDO
delete from WORKSTORE where HOSP_CODE = 'TWE' and WORKSTORE_CODE = 'WKS';
delete from WORKSTORE where HOSP_CODE = 'QEH' and WORKSTORE_CODE = 'WKSE';
delete from WORKSTORE where HOSP_CODE = 'YFS' and WORKSTORE_CODE = 'WKS';
--//
