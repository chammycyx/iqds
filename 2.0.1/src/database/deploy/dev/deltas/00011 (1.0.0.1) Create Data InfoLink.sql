insert into INFO_LINK (ID,NAME_TC,NAME,SORT_SEQ,URL) values (1,'醫院管理局','Hospital Authority',1,'http://www.ha.org.hk/visitor/ha_index.asp?Lang=CHIB5');
insert into INFO_LINK (ID,NAME_TC,NAME,SORT_SEQ,URL) values (2,'智友站','Smart Patient',2,'http://www21.ha.org.hk/smartpatient/tc/home.html');
insert into INFO_LINK (ID,NAME_TC,NAME,SORT_SEQ,URL) values (3,'衞生署','Department of Health',3,'http://www.dh.gov.hk/cindex.html');
insert into INFO_LINK (ID,NAME_TC,NAME,SORT_SEQ,URL) values (4,'衞生署藥物辦公室','Department of Health Drug Office',4,'http://www.drugoffice.gov.hk/eps/do/tc/level.html');


--//@UNDO
delete from INFO_LINK;
--//