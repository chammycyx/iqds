create table ACCESS_COUNT_LOG (
    TYPE varchar2(2) not null,
    NAME varchar2(1000) not null,
    LOG_DATE date not null,
    RECORD_COUNT number(10) not null,
    DEVICE_TYPE varchar2(1) not null,
    constraint PK_ACCESS_COUNT_LOG primary key (TYPE, NAME, LOG_DATE, DEVICE_TYPE) using index tablespace IQDS_LOG_INDX_01)
tablespace IQDS_LOG_INDX_01;

comment on table  ACCESS_COUNT_LOG is 'Consolidated statisitcs of retrieve counts';
comment on column ACCESS_COUNT_LOG.TYPE is 'Type of access action';
comment on column ACCESS_COUNT_LOG.NAME is 'Name of value for counting';
comment on column ACCESS_COUNT_LOG.LOG_DATE is 'Date of record access';
comment on column ACCESS_COUNT_LOG.RECORD_COUNT is 'Date of record access';
comment on column ACCESS_COUNT_LOG.DEVICE_TYPE is 'Platform of access device';

--//@UNDO
drop table ACCESS_COUNT_LOG;
--//