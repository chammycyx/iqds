create table DRUG_DOC (
    DRUG_NAME varchar2(100) not null,
    DRUG_DOC_MD clob null,
    DRUG_DOC_MD_CHI clob null,
    RELEASE_DATE timestamp null,
    constraint PK_DRUG_DOC primary key (DRUG_NAME) using index tablespace IQDS_LOG_INDX_01)
tablespace IQDS_LOG_INDX_01;

comment on table  DRUG_DOC is 'Drug document';
comment on column DRUG_DOC.DRUG_NAME is 'Drug name';
comment on column DRUG_DOC.DRUG_DOC_MD is 'Drug document markdown';
comment on column DRUG_DOC.DRUG_DOC_MD_CHI is 'Drug document markdown chinese';
comment on column DRUG_DOC.RELEASE_DATE is 'Release date';

--//@UNDO
drop table DRUG_DOC;
--//