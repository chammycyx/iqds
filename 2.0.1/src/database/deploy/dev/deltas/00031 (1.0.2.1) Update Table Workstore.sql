alter table WORKSTORE add WINDOW_TICKET_COUNT number(10) default 0 null;

update WORKSTORE set WINDOW_TICKET_COUNT = 0;

alter table WORKSTORE add WINDOW_TICKET_TASK_DATE timestamp null;

--//@UNDO
alter table WORKSTORE DROP column WINDOW_TICKET_COUNT;
alter table WORKSTORE DROP column WINDOW_TICKET_TASK_DATE;
--//