update WORKSTORE
set NAME = 'Yan Chai Hospital Block C Pharmacy',
NAME_TC = '仁濟醫院C座藥劑部',
ADDRESS = '1/F, Block C, Yan Chai Hospital, 7-11 Yan Chai Street, Tsuen Wan, New Territories',
ADDRESS_TC = '新界荃灣仁濟街 7-11 號仁濟醫院C座一樓',
SERVICE_HOUR = 'Monday to Friday:'||chr(10)||'9:00 AM to 11:00 PM'||chr(10)||''||chr(10)||'Saturday:'||chr(10)||'9:00 AM to 6:00 PM'||chr(10)||'7:00 PM to 11:00 PM'||chr(10)||''||chr(10)||'Sunday and Public Holidays:'||chr(10)||'9:00 AM to 1:00 PM'||chr(10)||'2:00 PM to 6:00 PM'||chr(10)||'7:00 PM to 11:00 PM',
SERVICE_HOUR_TC = '星期一至五:'||chr(10)||'上午 9:00 至下午 11:00'||chr(10)||''||chr(10)||'星期六:'||chr(10)||'上午 9:00 至下午 6:00'||chr(10)||'下午 7:00 至下午 11:00'||chr(10)||''||chr(10)||'星期日及公眾假期:'||chr(10)||'上午 9:00 至下午 1:00'||chr(10)||'下午 2:00 至下午 6:00'||chr(10)||'下午 7:00 至下午 11:00',
REMARK = 'Please collect medications on the same day when the prescription is received. The prescription is valid for 4 working days.'||chr(10)||''||chr(10)||'Please bring the prescription and settle the drug fee (if required) at the Shroff Counter before collecting the medications at the Pharmacy.'||chr(10)||'If financial aid is required, please contact a medical social worker.'||chr(10)||'If the currently serving number has exceeded yours by 30, you may enquire at counter no. 8 in Pharmacy.',
REMARK_TC = '請於領取藥單當天取藥，藥單有效期為四個工作天。'||chr(10)||''||chr(10)||'如需繳費，請先㩗藥單到繳費處繳付藥費，然後把藥單交往藥劑部取藥。'||chr(10)||''||chr(10)||'如有經濟困難，請聯絡醫務社工。'||chr(10)||'如藥劑部現在配發藥物的號碼已超越閣下的籌號三十個，請到藥劑部八號櫃位查詢。',
IMAGE_URL = '../../images/YCH_WKS2_02.png'
where HOSP_CODE = 'YCH' and WORKSTORE_CODE = 'WKS2';

--//@UNDO
--//
