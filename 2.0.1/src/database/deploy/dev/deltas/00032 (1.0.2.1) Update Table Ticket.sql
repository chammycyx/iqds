alter table TICKET add STATUS varchar2(1) null;

update TICKET set STATUS = 'A';

alter table TICKET modify STATUS not null;

--//@UNDO
alter table TICKET DROP column STATUS;
--//