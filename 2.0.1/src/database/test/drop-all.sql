ALTER TABLE PUSH_REQUEST DROP CONSTRAINT FK_PUSH_REQUEST_TICKET_ID
ALTER TABLE PUSH_REQUEST DROP CONSTRAINT FK_PUSH_REQUEST_DEVICE_ID
ALTER TABLE PUSH_REQUEST DROP CONSTRAINT FK_PUSH_REQUEST_WORKSTORE_CODE
ALTER TABLE TICKET DROP CONSTRAINT FK_TICKET_WORKSTORE_CODE
DROP TABLE DEVICE
DROP TABLE PUSH_REQUEST
DROP TABLE TICKET
DROP TABLE AUDIT_LOG
DROP TABLE INFO_LINK
DROP TABLE WORKSTORE
DROP SEQUENCE SQ_DEVICE
DROP SEQUENCE SQ_PUSH_REQUEST
DROP SEQUENCE SQ_AUDIT_LOG
DROP SEQUENCE SQ_TICKET
DROP SEQUENCE SQ_INFO_LINK
